<?php
/*
....
*/
require_once dirname( __FILE__ ).'/base.addon.php';

if( !class_exists( 'CPCFF_DropBox' ) )
{
    class CPCFF_DropBox extends CPCFF_BaseAddon
    {
        /************* ADDON SYSTEM - ATTRIBUTES AND METHODS *************/
		protected $addonID = "addon-dropbox-20161228";
		protected $name = "CFF - DropBox Integration";
		protected $description;

		public function get_addon_settings()
		{
			if( isset( $_REQUEST[ 'cpcff_dropbox' ] ) )
			{
				check_admin_referer( 'session_id_'.CP_SESSION::session_id(), '_cpcff_nonce' );

				$this->dropbox_token = trim( $_REQUEST['CP_CFF_DROPBOX_ACCESS_TOKEN'] );
				$this->remove_local  = ( isset( $_REQUEST['CP_CFF_DROPBOX_REMOVE_LOCAL'] ) ) ? true : false;

				update_option( 'CP_CFF_DROPBOX_ACCESS_TOKEN',  $this->dropbox_token );
				update_option( 'CP_CFF_DROPBOX_REMOVE_LOCAL',  $this->remove_local );
			}
			?>
			<form method="post">
				<div id="metabox_basic_settings" class="postbox" >
					<h3 class='hndle' style="padding:5px;"><span><?php print $this->name; ?></span></h3>
					<div class="inside">
						<p>
							<?php _e('Enter the DropBox Access Token', 'calculated-fields-form');?>:<br>
							<input name="CP_CFF_DROPBOX_ACCESS_TOKEN" type="text" style="width:100%;" value="<?php print esc_attr( $this->dropbox_token ); ?>" />
						</p>
						<p>
							To create a DropBox App and get the authentication token, please, <a href="https://www.dropbox.com/developers/apps/create" target="_blank">CLICK HERE</a> <a href="javascript:void(0);" onclick="jQuery('.CP_CFF_DROPBOX_HELP').show();">[?]</a>
						</p>
						<div class="CP_CFF_DROPBOX_HELP" style="border:1px solid #F0AD4E;background:#FBE6CA;padding:10px;display:none;">
							<div style="text-align:right;"><a href="javascript:void(0);" onclick="jQuery('.CP_CFF_DROPBOX_HELP').hide();">[x]</a></div>
							<div>
								<p>Access your account, to the reserved area where configure an App: <a href="https://www.dropbox.com/developers/apps/create" target="_blank">https://www.dropbox.com/developers/apps/create</a></p>
								<p>
									<img src="<?php print esc_attr(plugins_url('/dropbox.addon/assets/step1.png', __FILE__)); ?>" style="width:50%;" />
								</p>
								<p>
									<ol>
										<li>Select the "Dropbox API" option.</li>
										<li>Select the "App folder" option.</li>
										<li>Enter the application name.</li>
										<li>Press the "Create app" button.</li>
									</ol>
								</p>
								<p>In the next screen press the "Generate" button to generate an access token</p>
								<p>
									<img src="<?php print esc_attr(plugins_url('/dropbox.addon/assets/step2.png', __FILE__)); ?>" style="width:50%;" />
								</p>
								<p>Finally, copy the generated access token to be used with the add-on.</p>
								<p>
									<img src="<?php print esc_attr(plugins_url('/dropbox.addon/assets/step3.png', __FILE__)); ?>" style="width:50%;" />
								</p>
							</div>
						</div>
						<p>
							<input type="checkbox" name="CP_CFF_DROPBOX_REMOVE_LOCAL" <?php if( $this->remove_local ) print 'CHECKED'; ?> />&nbsp; <?php _e( 'Delete the local copy of file', 'calculated-fields-form' ); ?>
						</p>
						<p><input type="submit" name="<?php _e('Save settings', 'calculated-fields-form'); ?>" /></p>
					</div>
					<input type="hidden" name="cpcff_dropbox" value="1" />
					<input type="hidden" name="_cpcff_nonce" value="<?php echo wp_create_nonce( 'session_id_'.CP_SESSION::session_id() ); ?>" />
				</div>
			</form>
			<?php
		}

		/************************ ADDON CODE *****************************/
        /************************ ATTRIBUTES *****************************/

		private $dropbox_token 		= '';
		private $remove_local 		= false;

        /************************ CONSTRUCT *****************************/

        function __construct()
        {
			$this->description = __("The add-on allows to copy/move the uploaded files to DropBox", 'calculated-fields-form');

            // Check if the plugin is active
			if( !$this->addon_is_active() ) return;

			$this->remove_local  = get_option('CP_CFF_DROPBOX_REMOVE_LOCAL', false);
			$this->dropbox_token = get_option('CP_CFF_DROPBOX_ACCESS_TOKEN', '');

			add_action( 'cpcff_file_uploaded', array( &$this, 'uploaded_file' ), 9, 2 );
		} // End __construct

        /************************ PROTECTED METHODS *****************************/
		/************************ PRIVATE METHODS *****************************/
		private function _upload_file( $file )
		{
			if( !empty( $file ) && file_exists( $file ) )
			{

				$file_size = filesize( $file );
				$upload_dir = wp_upload_dir();
				$path = str_replace( '\\', '/', substr( $file, strlen( $upload_dir['basedir'] ) ) );

				$response = wp_remote_post(
					'https://content.dropboxapi.com/2/files/upload',
					array(
						'headers' => array(
							'Authorization' => 'Bearer '.$this->dropbox_token,
							'Content-Type' => 'application/octet-stream',
							'Dropbox-API-Arg' => json_encode(
								array(
									"path"=> $path,
									"mode" => "add",
									"autorename" => true,
									"mute" => false
								)
							)
						),
						'body' => @file_get_contents( $file )
					)
				);

				if( !is_wp_error( $response ) )
				{
					$json = json_decode(wp_remote_retrieve_body($response));
					if(
						!empty( $json ) &&
						!property_exists( $json, 'error')
					)
					{
						if( $json->size !== $file_size ) $this->_delete_file( $json->path_lower );
						else return $json;
					}
				}
				return false;
			}
		} // End _upload_file

		private function _delete_file( $path )
		{
			$delete_url = "https://api.dropboxapi.com/2/files/delete";
			$response = wp_remote_post(
				$delete_url,
				array(
					'headers' => array(
						'Authorization' => 'Bearer '.$this->dropbox_token,
						'Content-Type' => 'application/json'
					),
					'body' => json_encode(array("path"=> $path))
				)
			);
		} // End _delete_file

	 	/************************ PUBLIC METHODS  *****************************/

		public function uploaded_file( $fileData, $filesParams )
		{
			$response = $this->_upload_file( $fileData[ 'file' ] );
			if(
				$response !== false &&
				$this->remove_local
			)
			{
				$pos = count( $filesParams[ 'names' ] ) - 1;
				$filesParams[ 'links'][ $pos ] = $response->path_lower;
				$filesParams[ 'urls'][ $pos ]  = $response->path_lower;

				wp_delete_file( $fileData[ 'file' ] );

				remove_all_actions( 'cpcff_file_uploaded' );
				add_action( 'cpcff_file_uploaded', array( &$this, 'uploaded_file' ), 9, 2 );
			}
		} // End uploaded_file

	} // End Class

    // Main add-on code
    $cpcff_google_places_obj = new CPCFF_DropBox();

	// Add addon object to the objects list
	CPCFF_ADDONS::add($cpcff_google_places_obj);
}
?>