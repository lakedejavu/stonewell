<?php
/*
....
*/
require_once dirname( __FILE__ ).'/base.addon.php';

if( !class_exists( 'CPCFF_PayPalPro' ) )
{
    class CPCFF_PayPalPro extends CPCFF_BaseAddon
    {

        /************* ADDON SYSTEM - ATTRIBUTES AND METHODS *************/
		protected $addonID = "addon-paypalpro-20151212";
		protected $name = "CFF - PayPal Pro";
		protected $description;

		public function get_addon_form_settings( $form_id )
		{
			global $wpdb;

			// Insertion in database
			if(
				isset( $_REQUEST[ 'cpcff_paypalpro_id' ] )
			)
			{
                $this->add_field_verify('paypalpro_default_country');
			    $wpdb->delete( $wpdb->prefix.$this->form_table, array( 'formid' => $form_id ), array( '%d' ) );
				$wpdb->insert(
								$wpdb->prefix.$this->form_table,
								array(
									'formid' => $form_id,
									'paypalpro_api_username'	 => $_REQUEST["paypalpro_api_username"],
									'paypalpro_api_password'	 => $_REQUEST["paypalpro_api_password"],
									'paypalpro_api_signature'	 => $_REQUEST["paypalpro_api_signature"],
                                    'paypalpro_api_bperiod'	     => $_REQUEST["paypalpro_api_bperiod"],
                                    'paypalpro_default_country'	 => $_REQUEST["paypalpro_default_country"],
									'currency'	 => $_REQUEST["currency"],
									'enabled'	 => $_REQUEST["enabled"],
									'paypal_mode'	 => $_REQUEST["paypal_modepp"]
								),
								array( '%d', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s' )
							);
			}


			$rows = $wpdb->get_results(
						$wpdb->prepare( "SELECT * FROM ".$wpdb->prefix.$this->form_table." WHERE formid=%d", $form_id )
					);
			if (!count($rows))
			{
			    $row["paypalpro_api_username"] = "";
			    $row["paypalpro_api_password"] = "";
			    $row["paypalpro_api_signature"] = "";
                $row["paypalpro_api_bperiod"] = "";
                $row["paypalpro_default_country"] = "GB";
			    $row["currency"] = "USD";
			    $row["enabled"] = "0";
			    $row["paypal_mode"] = "production";
			} else {
			    $row["paypalpro_api_username"] = $rows[0]->paypalpro_api_username;
			    $row["paypalpro_api_password"] = $rows[0]->paypalpro_api_password;
			    $row["paypalpro_api_signature"] = $rows[0]->paypalpro_api_signature;
                $row["paypalpro_api_bperiod"] = $rows[0]->paypalpro_api_bperiod;
                $row["paypalpro_default_country"] = $rows[0]->paypalpro_default_country;
                if (empty($row["paypalpro_default_country"]))
                    $row["paypalpro_default_country"] = 'GB';
			    $row["currency"] = $rows[0]->currency;
			    $row["enabled"] = $rows[0]->enabled;
			    $row["paypal_mode"] = $rows[0]->paypal_mode;
			}

			?>
			<div id="metabox_basic_settings" class="postbox" >
				<h3 class='hndle' style="padding:5px;"><span><?php print $this->name; ?></span></h3>
				<div class="inside">
				   <input type="hidden" name="cpcff_paypalpro_id" value="1" />
                   <table class="form-table">
                    <tr valign="top">
                    <th scope="row"><?php _e('Enable PayPal Pro? (if enabled PayPal Standard is disabled)', 'calculated-fields-form'); ?></th>
                    <td><select name="enabled">
                         <option value="0" <?php if (!$row["enabled"]) echo 'selected'; ?>><?php _e('No', 'calculated-fields-form'); ?></option>
                         <option value="1" <?php if ($row["enabled"] == '1') echo 'selected'; ?>><?php _e('Yes', 'calculated-fields-form'); ?></option>
                         <option value="2" <?php if ($row["enabled"] == '2') echo 'selected'; ?>><?php _e('Optional: This payment method + Pay Later (submit without payment)', 'calculated-fields-form'); ?></option>
                         <option value="3" <?php if ($row["enabled"] == '3') echo 'selected'; ?>><?php _e('Optional: This payment method + Other payment methods (enabled)', 'calculated-fields-form'); ?></option>
                         <option value="4" <?php if ($row["enabled"] == '4') echo 'selected'; ?>><?php _e('Optional: This payment method + Other payment methods  + Pay Later ', 'calculated-fields-form'); ?></option>
                         </select>
                    </td>
                    </tr>
                    <tr valign="top">
                    <th scope="row"><?php _e('PayPal Pro - API UserName', 'calculated-fields-form'); ?></th>
                    <td><input type="text" name="paypalpro_api_username" size="20" value="<?php echo esc_attr($row["paypalpro_api_username"]); ?>" /></td>
                    </tr>
                    <tr valign="top">
                    <th scope="row"><?php _e('PayPal Pro - API Password', 'calculated-fields-form');?></th>
                    <td><input type="text" name="paypalpro_api_password" size="40" value="<?php echo esc_attr($row["paypalpro_api_password"]); ?>" /></td>
                    </tr>
                    <tr valign="top">
                    <th scope="row"><?php _e('PayPal Pro - API Signature', 'calculated-fields-form'); ?></th>
                    <td><input type="text" name="paypalpro_api_signature" size="20" value="<?php echo esc_attr($row["paypalpro_api_signature"]); ?>" /></td>
                    </tr>
                    <tr valign="top">
                    <th scope="row"><?php _e('PayPal Pro - Currency', 'calculated-fields-form'); ?></th>
                    <td><input type="text" name="currency" size="20" value="<?php echo esc_attr($row["currency"]); ?>" /></td>
                    </tr>
                    <tr valign="top">
                    <th scope="row"><?php _e('Billing Period', 'calculated-fields-form'); ?></th>
                    <td><select name="paypalpro_api_bperiod">
                         <option value="" <?php if ($row["paypalpro_api_bperiod"] == '') echo 'selected'; ?>><?php _e('One-time payment', 'calculated-fields-form'); ?></option>
                         <option value="Day" <?php if ($row["paypalpro_api_bperiod"] == 'Day') echo 'selected'; ?>><?php _e('Daily', 'calculated-fields-form'); ?></option>
                         <option value="Week" <?php if ($row["paypalpro_api_bperiod"] == 'Week') echo 'selected'; ?>><?php _e('Weekly', 'calculated-fields-form'); ?></option>
                         <option value="SemiMonth" <?php if ($row["paypalpro_api_bperiod"] == 'SemiMonth') echo 'selected'; ?>><?php _e('SemiMonth', 'calculated-fields-form'); ?></option>
                         <option value="Month" <?php if ($row["paypalpro_api_bperiod"] == 'Month') echo 'selected'; ?>><?php _e('Monthly', 'calculated-fields-form'); ?></option>
                         <option value="Year" <?php if ($row["paypalpro_api_bperiod"] == 'Year') echo 'selected'; ?>><?php _e('Yearly', 'calculated-fields-form'); ?></option>
                        </select>
                    </td>
                    </tr>
                    <tr valign="top">
                    <th scope="row"><?php _e('Paypal Mode', 'calculated-fields-form'); ?></th>
                    <td><select name="paypal_modepp">
                         <option value="production" <?php if ($row["paypal_mode"] != 'sandbox') echo 'selected'; ?>><?php _e('Production - real payments processed', 'calculated-fields-form'); ?></option>
                         <option value="sandbox" <?php if ($row["paypal_mode"] == 'sandbox') echo 'selected'; ?>><?php _e('SandBox - PayPal testing sandbox area', 'calculated-fields-form'); ?></option>
                        </select>
                    </td>
                    </tr>
                    <tr valign="top">
                    <th scope="row"><?php _e('Default Country', 'calculated-fields-form'); ?></th>
                    <td><select name="paypalpro_default_country">
	<option value="AF"<?php if ($row["paypalpro_default_country"] == 'AF') echo ' selected'; ?>>Afghanistan</option>
	<option value="AX"<?php if ($row["paypalpro_default_country"] == 'AX') echo ' selected'; ?>>�land Islands</option>
	<option value="AL"<?php if ($row["paypalpro_default_country"] == 'AL') echo ' selected'; ?>>Albania</option>
	<option value="DZ"<?php if ($row["paypalpro_default_country"] == 'DZ') echo ' selected'; ?>>Algeria</option>
	<option value="AS"<?php if ($row["paypalpro_default_country"] == 'AS') echo ' selected'; ?>>American Samoa</option>
	<option value="AD"<?php if ($row["paypalpro_default_country"] == 'AD') echo ' selected'; ?>>Andorra</option>
	<option value="AO"<?php if ($row["paypalpro_default_country"] == 'AO') echo ' selected'; ?>>Angola</option>
	<option value="AI"<?php if ($row["paypalpro_default_country"] == 'AI') echo ' selected'; ?>>Anguilla</option>
	<option value="AQ"<?php if ($row["paypalpro_default_country"] == 'AQ') echo ' selected'; ?>>Antarctica</option>
	<option value="AG"<?php if ($row["paypalpro_default_country"] == 'AG') echo ' selected'; ?>>Antigua and Barbuda</option>
	<option value="AR"<?php if ($row["paypalpro_default_country"] == 'AR') echo ' selected'; ?>>Argentina</option>
	<option value="AM"<?php if ($row["paypalpro_default_country"] == 'AM') echo ' selected'; ?>>Armenia</option>
	<option value="AW"<?php if ($row["paypalpro_default_country"] == 'AW') echo ' selected'; ?>>Aruba</option>
	<option value="AU"<?php if ($row["paypalpro_default_country"] == 'AU') echo ' selected'; ?>>Australia</option>
	<option value="AT"<?php if ($row["paypalpro_default_country"] == 'AT') echo ' selected'; ?>>Austria</option>
	<option value="AZ"<?php if ($row["paypalpro_default_country"] == 'AZ') echo ' selected'; ?>>Azerbaijan</option>
	<option value="BS"<?php if ($row["paypalpro_default_country"] == 'BS') echo ' selected'; ?>>Bahamas</option>
	<option value="BH"<?php if ($row["paypalpro_default_country"] == 'BH') echo ' selected'; ?>>Bahrain</option>
	<option value="BD"<?php if ($row["paypalpro_default_country"] == 'BD') echo ' selected'; ?>>Bangladesh</option>
	<option value="BB"<?php if ($row["paypalpro_default_country"] == 'BB') echo ' selected'; ?>>Barbados</option>
	<option value="BY"<?php if ($row["paypalpro_default_country"] == 'BY') echo ' selected'; ?>>Belarus</option>
	<option value="BE"<?php if ($row["paypalpro_default_country"] == 'BE') echo ' selected'; ?>>Belgium</option>
	<option value="BZ"<?php if ($row["paypalpro_default_country"] == 'BZ') echo ' selected'; ?>>Belize</option>
	<option value="BJ"<?php if ($row["paypalpro_default_country"] == 'BJ') echo ' selected'; ?>>Benin</option>
	<option value="BM"<?php if ($row["paypalpro_default_country"] == 'BM') echo ' selected'; ?>>Bermuda</option>
	<option value="BT"<?php if ($row["paypalpro_default_country"] == 'BT') echo ' selected'; ?>>Bhutan</option>
	<option value="BO"<?php if ($row["paypalpro_default_country"] == 'BO') echo ' selected'; ?>>Bolivia, Plurinational State of</option>
	<option value="BQ"<?php if ($row["paypalpro_default_country"] == 'BQ') echo ' selected'; ?>>Bonaire, Sint Eustatius and Saba</option>
	<option value="BA"<?php if ($row["paypalpro_default_country"] == 'BA') echo ' selected'; ?>>Bosnia and Herzegovina</option>
	<option value="BW"<?php if ($row["paypalpro_default_country"] == 'BW') echo ' selected'; ?>>Botswana</option>
	<option value="BV"<?php if ($row["paypalpro_default_country"] == 'BV') echo ' selected'; ?>>Bouvet Island</option>
	<option value="BR"<?php if ($row["paypalpro_default_country"] == 'BR') echo ' selected'; ?>>Brazil</option>
	<option value="IO"<?php if ($row["paypalpro_default_country"] == 'IO') echo ' selected'; ?>>British Indian Ocean Territory</option>
	<option value="BN"<?php if ($row["paypalpro_default_country"] == 'BN') echo ' selected'; ?>>Brunei Darussalam</option>
	<option value="BG"<?php if ($row["paypalpro_default_country"] == 'BG') echo ' selected'; ?>>Bulgaria</option>
	<option value="BF"<?php if ($row["paypalpro_default_country"] == 'BF') echo ' selected'; ?>>Burkina Faso</option>
	<option value="BI"<?php if ($row["paypalpro_default_country"] == 'BI') echo ' selected'; ?>>Burundi</option>
	<option value="KH"<?php if ($row["paypalpro_default_country"] == 'KH') echo ' selected'; ?>>Cambodia</option>
	<option value="CM"<?php if ($row["paypalpro_default_country"] == 'CM') echo ' selected'; ?>>Cameroon</option>
	<option value="CA"<?php if ($row["paypalpro_default_country"] == 'CA') echo ' selected'; ?>>Canada</option>
	<option value="CV"<?php if ($row["paypalpro_default_country"] == 'CV') echo ' selected'; ?>>Cape Verde</option>
	<option value="KY"<?php if ($row["paypalpro_default_country"] == 'KY') echo ' selected'; ?>>Cayman Islands</option>
	<option value="CF"<?php if ($row["paypalpro_default_country"] == 'CF') echo ' selected'; ?>>Central African Republic</option>
	<option value="TD"<?php if ($row["paypalpro_default_country"] == 'TD') echo ' selected'; ?>>Chad</option>
	<option value="CL"<?php if ($row["paypalpro_default_country"] == 'CL') echo ' selected'; ?>>Chile</option>
	<option value="CN"<?php if ($row["paypalpro_default_country"] == 'CN') echo ' selected'; ?>>China</option>
	<option value="CX"<?php if ($row["paypalpro_default_country"] == 'CX') echo ' selected'; ?>>Christmas Island</option>
	<option value="CC"<?php if ($row["paypalpro_default_country"] == 'CC') echo ' selected'; ?>>Cocos (Keeling) Islands</option>
	<option value="CO"<?php if ($row["paypalpro_default_country"] == 'CO') echo ' selected'; ?>>Colombia</option>
	<option value="KM"<?php if ($row["paypalpro_default_country"] == 'KM') echo ' selected'; ?>>Comoros</option>
	<option value="CG"<?php if ($row["paypalpro_default_country"] == 'CG') echo ' selected'; ?>>Congo</option>
	<option value="CD"<?php if ($row["paypalpro_default_country"] == 'CD') echo ' selected'; ?>>Congo, the Democratic Republic of the</option>
	<option value="CK"<?php if ($row["paypalpro_default_country"] == 'CK') echo ' selected'; ?>>Cook Islands</option>
	<option value="CR"<?php if ($row["paypalpro_default_country"] == 'CR') echo ' selected'; ?>>Costa Rica</option>
	<option value="CI"<?php if ($row["paypalpro_default_country"] == 'CI') echo ' selected'; ?>>C�te d'Ivoire</option>
	<option value="HR"<?php if ($row["paypalpro_default_country"] == 'HR') echo ' selected'; ?>>Croatia</option>
	<option value="CU"<?php if ($row["paypalpro_default_country"] == 'CU') echo ' selected'; ?>>Cuba</option>
	<option value="CW"<?php if ($row["paypalpro_default_country"] == 'CW') echo ' selected'; ?>>Cura�ao</option>
	<option value="CY"<?php if ($row["paypalpro_default_country"] == 'CY') echo ' selected'; ?>>Cyprus</option>
	<option value="CZ"<?php if ($row["paypalpro_default_country"] == 'CZ') echo ' selected'; ?>>Czech Republic</option>
	<option value="DK"<?php if ($row["paypalpro_default_country"] == 'DK') echo ' selected'; ?>>Denmark</option>
	<option value="DJ"<?php if ($row["paypalpro_default_country"] == 'DJ') echo ' selected'; ?>>Djibouti</option>
	<option value="DM"<?php if ($row["paypalpro_default_country"] == 'DM') echo ' selected'; ?>>Dominica</option>
	<option value="DO"<?php if ($row["paypalpro_default_country"] == 'DO') echo ' selected'; ?>>Dominican Republic</option>
	<option value="EC"<?php if ($row["paypalpro_default_country"] == 'EC') echo ' selected'; ?>>Ecuador</option>
	<option value="EG"<?php if ($row["paypalpro_default_country"] == 'EG') echo ' selected'; ?>>Egypt</option>
	<option value="SV"<?php if ($row["paypalpro_default_country"] == 'SV') echo ' selected'; ?>>El Salvador</option>
	<option value="GQ"<?php if ($row["paypalpro_default_country"] == 'GQ') echo ' selected'; ?>>Equatorial Guinea</option>
	<option value="ER"<?php if ($row["paypalpro_default_country"] == 'ER') echo ' selected'; ?>>Eritrea</option>
	<option value="EE"<?php if ($row["paypalpro_default_country"] == 'EE') echo ' selected'; ?>>Estonia</option>
	<option value="ET"<?php if ($row["paypalpro_default_country"] == 'ET') echo ' selected'; ?>>Ethiopia</option>
	<option value="FK"<?php if ($row["paypalpro_default_country"] == 'FK') echo ' selected'; ?>>Falkland Islands (Malvinas)</option>
	<option value="FO"<?php if ($row["paypalpro_default_country"] == 'FO') echo ' selected'; ?>>Faroe Islands</option>
	<option value="FJ"<?php if ($row["paypalpro_default_country"] == 'FJ') echo ' selected'; ?>>Fiji</option>
	<option value="FI"<?php if ($row["paypalpro_default_country"] == 'FI') echo ' selected'; ?>>Finland</option>
	<option value="FR"<?php if ($row["paypalpro_default_country"] == 'FR') echo ' selected'; ?>>France</option>
	<option value="GF"<?php if ($row["paypalpro_default_country"] == 'GF') echo ' selected'; ?>>French Guiana</option>
	<option value="PF"<?php if ($row["paypalpro_default_country"] == 'PF') echo ' selected'; ?>>French Polynesia</option>
	<option value="TF"<?php if ($row["paypalpro_default_country"] == 'TF') echo ' selected'; ?>>French Southern Territories</option>
	<option value="GA"<?php if ($row["paypalpro_default_country"] == 'GA') echo ' selected'; ?>>Gabon</option>
	<option value="GM"<?php if ($row["paypalpro_default_country"] == 'GM') echo ' selected'; ?>>Gambia</option>
	<option value="GE"<?php if ($row["paypalpro_default_country"] == 'GE') echo ' selected'; ?>>Georgia</option>
	<option value="DE"<?php if ($row["paypalpro_default_country"] == 'DE') echo ' selected'; ?>>Germany</option>
	<option value="GH"<?php if ($row["paypalpro_default_country"] == 'GH') echo ' selected'; ?>>Ghana</option>
	<option value="GI"<?php if ($row["paypalpro_default_country"] == 'GI') echo ' selected'; ?>>Gibraltar</option>
	<option value="GR"<?php if ($row["paypalpro_default_country"] == 'GR') echo ' selected'; ?>>Greece</option>
	<option value="GL"<?php if ($row["paypalpro_default_country"] == 'GL') echo ' selected'; ?>>Greenland</option>
	<option value="GD"<?php if ($row["paypalpro_default_country"] == 'GD') echo ' selected'; ?>>Grenada</option>
	<option value="GP"<?php if ($row["paypalpro_default_country"] == 'GP') echo ' selected'; ?>>Guadeloupe</option>
	<option value="GU"<?php if ($row["paypalpro_default_country"] == 'GU') echo ' selected'; ?>>Guam</option>
	<option value="GT"<?php if ($row["paypalpro_default_country"] == 'GT') echo ' selected'; ?>>Guatemala</option>
	<option value="GG"<?php if ($row["paypalpro_default_country"] == 'GG') echo ' selected'; ?>>Guernsey</option>
	<option value="GN"<?php if ($row["paypalpro_default_country"] == 'GN') echo ' selected'; ?>>Guinea</option>
	<option value="GW"<?php if ($row["paypalpro_default_country"] == 'GW') echo ' selected'; ?>>Guinea-Bissau</option>
	<option value="GY"<?php if ($row["paypalpro_default_country"] == 'GY') echo ' selected'; ?>>Guyana</option>
	<option value="HT"<?php if ($row["paypalpro_default_country"] == 'HT') echo ' selected'; ?>>Haiti</option>
	<option value="HM"<?php if ($row["paypalpro_default_country"] == 'HM') echo ' selected'; ?>>Heard Island and McDonald Islands</option>
	<option value="VA"<?php if ($row["paypalpro_default_country"] == 'VA') echo ' selected'; ?>>Holy See (Vatican City State)</option>
	<option value="HN"<?php if ($row["paypalpro_default_country"] == 'HN') echo ' selected'; ?>>Honduras</option>
	<option value="HK"<?php if ($row["paypalpro_default_country"] == 'HK') echo ' selected'; ?>>Hong Kong</option>
	<option value="HU"<?php if ($row["paypalpro_default_country"] == 'HU') echo ' selected'; ?>>Hungary</option>
	<option value="IS"<?php if ($row["paypalpro_default_country"] == 'IS') echo ' selected'; ?>>Iceland</option>
	<option value="IN"<?php if ($row["paypalpro_default_country"] == 'IN') echo ' selected'; ?>>India</option>
	<option value="ID"<?php if ($row["paypalpro_default_country"] == 'ID') echo ' selected'; ?>>Indonesia</option>
	<option value="IR"<?php if ($row["paypalpro_default_country"] == 'IR') echo ' selected'; ?>>Iran, Islamic Republic of</option>
	<option value="IQ"<?php if ($row["paypalpro_default_country"] == 'IQ') echo ' selected'; ?>>Iraq</option>
	<option value="IE"<?php if ($row["paypalpro_default_country"] == 'IE') echo ' selected'; ?>>Ireland</option>
	<option value="IM"<?php if ($row["paypalpro_default_country"] == 'IM') echo ' selected'; ?>>Isle of Man</option>
	<option value="IL"<?php if ($row["paypalpro_default_country"] == 'IL') echo ' selected'; ?>>Israel</option>
	<option value="IT"<?php if ($row["paypalpro_default_country"] == 'IT') echo ' selected'; ?>>Italy</option>
	<option value="JM"<?php if ($row["paypalpro_default_country"] == 'JM') echo ' selected'; ?>>Jamaica</option>
	<option value="JP"<?php if ($row["paypalpro_default_country"] == 'JP') echo ' selected'; ?>>Japan</option>
	<option value="JE"<?php if ($row["paypalpro_default_country"] == 'JE') echo ' selected'; ?>>Jersey</option>
	<option value="JO"<?php if ($row["paypalpro_default_country"] == 'JO') echo ' selected'; ?>>Jordan</option>
	<option value="KZ"<?php if ($row["paypalpro_default_country"] == 'KZ') echo ' selected'; ?>>Kazakhstan</option>
	<option value="KE"<?php if ($row["paypalpro_default_country"] == 'KE') echo ' selected'; ?>>Kenya</option>
	<option value="KI"<?php if ($row["paypalpro_default_country"] == 'KI') echo ' selected'; ?>>Kiribati</option>
	<option value="KP"<?php if ($row["paypalpro_default_country"] == 'KP') echo ' selected'; ?>>Korea, Democratic People's Republic of</option>
	<option value="KR"<?php if ($row["paypalpro_default_country"] == 'KR') echo ' selected'; ?>>Korea, Republic of</option>
	<option value="KW"<?php if ($row["paypalpro_default_country"] == 'KW') echo ' selected'; ?>>Kuwait</option>
	<option value="KG"<?php if ($row["paypalpro_default_country"] == 'KG') echo ' selected'; ?>>Kyrgyzstan</option>
	<option value="LA"<?php if ($row["paypalpro_default_country"] == 'LA') echo ' selected'; ?>>Lao People's Democratic Republic</option>
	<option value="LV"<?php if ($row["paypalpro_default_country"] == 'LV') echo ' selected'; ?>>Latvia</option>
	<option value="LB"<?php if ($row["paypalpro_default_country"] == 'LB') echo ' selected'; ?>>Lebanon</option>
	<option value="LS"<?php if ($row["paypalpro_default_country"] == 'LS') echo ' selected'; ?>>Lesotho</option>
	<option value="LR"<?php if ($row["paypalpro_default_country"] == 'LR') echo ' selected'; ?>>Liberia</option>
	<option value="LY"<?php if ($row["paypalpro_default_country"] == 'LY') echo ' selected'; ?>>Libya</option>
	<option value="LI"<?php if ($row["paypalpro_default_country"] == 'LI') echo ' selected'; ?>>Liechtenstein</option>
	<option value="LT"<?php if ($row["paypalpro_default_country"] == 'LT') echo ' selected'; ?>>Lithuania</option>
	<option value="LU"<?php if ($row["paypalpro_default_country"] == 'LU') echo ' selected'; ?>>Luxembourg</option>
	<option value="MO"<?php if ($row["paypalpro_default_country"] == 'MO') echo ' selected'; ?>>Macao</option>
	<option value="MK"<?php if ($row["paypalpro_default_country"] == 'MK') echo ' selected'; ?>>Macedonia, the former Yugoslav Republic of</option>
	<option value="MG"<?php if ($row["paypalpro_default_country"] == 'MG') echo ' selected'; ?>>Madagascar</option>
	<option value="MW"<?php if ($row["paypalpro_default_country"] == 'MW') echo ' selected'; ?>>Malawi</option>
	<option value="MY"<?php if ($row["paypalpro_default_country"] == 'MY') echo ' selected'; ?>>Malaysia</option>
	<option value="MV"<?php if ($row["paypalpro_default_country"] == 'MV') echo ' selected'; ?>>Maldives</option>
	<option value="ML"<?php if ($row["paypalpro_default_country"] == 'ML') echo ' selected'; ?>>Mali</option>
	<option value="MT"<?php if ($row["paypalpro_default_country"] == 'MT') echo ' selected'; ?>>Malta</option>
	<option value="MH"<?php if ($row["paypalpro_default_country"] == 'MH') echo ' selected'; ?>>Marshall Islands</option>
	<option value="MQ"<?php if ($row["paypalpro_default_country"] == 'MQ') echo ' selected'; ?>>Martinique</option>
	<option value="MR"<?php if ($row["paypalpro_default_country"] == 'MR') echo ' selected'; ?>>Mauritania</option>
	<option value="MU"<?php if ($row["paypalpro_default_country"] == 'MU') echo ' selected'; ?>>Mauritius</option>
	<option value="YT"<?php if ($row["paypalpro_default_country"] == 'YT') echo ' selected'; ?>>Mayotte</option>
	<option value="MX"<?php if ($row["paypalpro_default_country"] == 'MX') echo ' selected'; ?>>Mexico</option>
	<option value="FM"<?php if ($row["paypalpro_default_country"] == 'FM') echo ' selected'; ?>>Micronesia, Federated States of</option>
	<option value="MD"<?php if ($row["paypalpro_default_country"] == 'MD') echo ' selected'; ?>>Moldova, Republic of</option>
	<option value="MC"<?php if ($row["paypalpro_default_country"] == 'MC') echo ' selected'; ?>>Monaco</option>
	<option value="MN"<?php if ($row["paypalpro_default_country"] == 'MN') echo ' selected'; ?>>Mongolia</option>
	<option value="ME"<?php if ($row["paypalpro_default_country"] == 'ME') echo ' selected'; ?>>Montenegro</option>
	<option value="MS"<?php if ($row["paypalpro_default_country"] == 'MS') echo ' selected'; ?>>Montserrat</option>
	<option value="MA"<?php if ($row["paypalpro_default_country"] == 'MA') echo ' selected'; ?>>Morocco</option>
	<option value="MZ"<?php if ($row["paypalpro_default_country"] == 'MZ') echo ' selected'; ?>>Mozambique</option>
	<option value="MM"<?php if ($row["paypalpro_default_country"] == 'MM') echo ' selected'; ?>>Myanmar</option>
	<option value="NA"<?php if ($row["paypalpro_default_country"] == 'NA') echo ' selected'; ?>>Namibia</option>
	<option value="NR"<?php if ($row["paypalpro_default_country"] == 'NR') echo ' selected'; ?>>Nauru</option>
	<option value="NP"<?php if ($row["paypalpro_default_country"] == 'NP') echo ' selected'; ?>>Nepal</option>
	<option value="NL"<?php if ($row["paypalpro_default_country"] == 'NL') echo ' selected'; ?>>Netherlands</option>
	<option value="NC"<?php if ($row["paypalpro_default_country"] == 'NC') echo ' selected'; ?>>New Caledonia</option>
	<option value="NZ"<?php if ($row["paypalpro_default_country"] == 'NZ') echo ' selected'; ?>>New Zealand</option>
	<option value="NI"<?php if ($row["paypalpro_default_country"] == 'NI') echo ' selected'; ?>>Nicaragua</option>
	<option value="NE"<?php if ($row["paypalpro_default_country"] == 'NE') echo ' selected'; ?>>Niger</option>
	<option value="NG"<?php if ($row["paypalpro_default_country"] == 'NG') echo ' selected'; ?>>Nigeria</option>
	<option value="NU"<?php if ($row["paypalpro_default_country"] == 'NU') echo ' selected'; ?>>Niue</option>
	<option value="NF"<?php if ($row["paypalpro_default_country"] == 'NF') echo ' selected'; ?>>Norfolk Island</option>
	<option value="MP"<?php if ($row["paypalpro_default_country"] == 'MP') echo ' selected'; ?>>Northern Mariana Islands</option>
	<option value="NO"<?php if ($row["paypalpro_default_country"] == 'NO') echo ' selected'; ?>>Norway</option>
	<option value="OM"<?php if ($row["paypalpro_default_country"] == 'OM') echo ' selected'; ?>>Oman</option>
	<option value="PK"<?php if ($row["paypalpro_default_country"] == 'PK') echo ' selected'; ?>>Pakistan</option>
	<option value="PW"<?php if ($row["paypalpro_default_country"] == 'PW') echo ' selected'; ?>>Palau</option>
	<option value="PS"<?php if ($row["paypalpro_default_country"] == 'PS') echo ' selected'; ?>>Palestinian Territory, Occupied</option>
	<option value="PA"<?php if ($row["paypalpro_default_country"] == 'PA') echo ' selected'; ?>>Panama</option>
	<option value="PG"<?php if ($row["paypalpro_default_country"] == 'PG') echo ' selected'; ?>>Papua New Guinea</option>
	<option value="PY"<?php if ($row["paypalpro_default_country"] == 'PY') echo ' selected'; ?>>Paraguay</option>
	<option value="PE"<?php if ($row["paypalpro_default_country"] == 'PE') echo ' selected'; ?>>Peru</option>
	<option value="PH"<?php if ($row["paypalpro_default_country"] == 'PH') echo ' selected'; ?>>Philippines</option>
	<option value="PN"<?php if ($row["paypalpro_default_country"] == 'PN') echo ' selected'; ?>>Pitcairn</option>
	<option value="PL"<?php if ($row["paypalpro_default_country"] == 'PL') echo ' selected'; ?>>Poland</option>
	<option value="PT"<?php if ($row["paypalpro_default_country"] == 'PT') echo ' selected'; ?>>Portugal</option>
	<option value="PR"<?php if ($row["paypalpro_default_country"] == 'PR') echo ' selected'; ?>>Puerto Rico</option>
	<option value="QA"<?php if ($row["paypalpro_default_country"] == 'QA') echo ' selected'; ?>>Qatar</option>
	<option value="RE"<?php if ($row["paypalpro_default_country"] == 'RE') echo ' selected'; ?>>R�union</option>
	<option value="RO"<?php if ($row["paypalpro_default_country"] == 'RO') echo ' selected'; ?>>Romania</option>
	<option value="RU"<?php if ($row["paypalpro_default_country"] == 'RU') echo ' selected'; ?>>Russian Federation</option>
	<option value="RW"<?php if ($row["paypalpro_default_country"] == 'RW') echo ' selected'; ?>>Rwanda</option>
	<option value="BL"<?php if ($row["paypalpro_default_country"] == 'BL') echo ' selected'; ?>>Saint Barth�lemy</option>
	<option value="SH"<?php if ($row["paypalpro_default_country"] == 'SH') echo ' selected'; ?>>Saint Helena, Ascension and Tristan da Cunha</option>
	<option value="KN"<?php if ($row["paypalpro_default_country"] == 'KN') echo ' selected'; ?>>Saint Kitts and Nevis</option>
	<option value="LC"<?php if ($row["paypalpro_default_country"] == 'LC') echo ' selected'; ?>>Saint Lucia</option>
	<option value="MF"<?php if ($row["paypalpro_default_country"] == 'MF') echo ' selected'; ?>>Saint Martin (French part)</option>
	<option value="PM"<?php if ($row["paypalpro_default_country"] == 'PM') echo ' selected'; ?>>Saint Pierre and Miquelon</option>
	<option value="VC"<?php if ($row["paypalpro_default_country"] == 'VC') echo ' selected'; ?>>Saint Vincent and the Grenadines</option>
	<option value="WS"<?php if ($row["paypalpro_default_country"] == 'WS') echo ' selected'; ?>>Samoa</option>
	<option value="SM"<?php if ($row["paypalpro_default_country"] == 'SM') echo ' selected'; ?>>San Marino</option>
	<option value="ST"<?php if ($row["paypalpro_default_country"] == 'ST') echo ' selected'; ?>>Sao Tome and Principe</option>
	<option value="SA"<?php if ($row["paypalpro_default_country"] == 'SA') echo ' selected'; ?>>Saudi Arabia</option>
	<option value="SN"<?php if ($row["paypalpro_default_country"] == 'SN') echo ' selected'; ?>>Senegal</option>
	<option value="RS"<?php if ($row["paypalpro_default_country"] == 'RS') echo ' selected'; ?>>Serbia</option>
	<option value="SC"<?php if ($row["paypalpro_default_country"] == 'SC') echo ' selected'; ?>>Seychelles</option>
	<option value="SL"<?php if ($row["paypalpro_default_country"] == 'SL') echo ' selected'; ?>>Sierra Leone</option>
	<option value="SG"<?php if ($row["paypalpro_default_country"] == 'SG') echo ' selected'; ?>>Singapore</option>
	<option value="SX"<?php if ($row["paypalpro_default_country"] == 'SX') echo ' selected'; ?>>Sint Maarten (Dutch part)</option>
	<option value="SK"<?php if ($row["paypalpro_default_country"] == 'SK') echo ' selected'; ?>>Slovakia</option>
	<option value="SI"<?php if ($row["paypalpro_default_country"] == 'SI') echo ' selected'; ?>>Slovenia</option>
	<option value="SB"<?php if ($row["paypalpro_default_country"] == 'SB') echo ' selected'; ?>>Solomon Islands</option>
	<option value="SO"<?php if ($row["paypalpro_default_country"] == 'SO') echo ' selected'; ?>>Somalia</option>
	<option value="ZA"<?php if ($row["paypalpro_default_country"] == 'ZA') echo ' selected'; ?>>South Africa</option>
	<option value="GS"<?php if ($row["paypalpro_default_country"] == 'GS') echo ' selected'; ?>>South Georgia and the South Sandwich Islands</option>
	<option value="SS"<?php if ($row["paypalpro_default_country"] == 'SS') echo ' selected'; ?>>South Sudan</option>
	<option value="ES"<?php if ($row["paypalpro_default_country"] == 'ES') echo ' selected'; ?>>Spain</option>
	<option value="LK"<?php if ($row["paypalpro_default_country"] == 'LK') echo ' selected'; ?>>Sri Lanka</option>
	<option value="SD"<?php if ($row["paypalpro_default_country"] == 'SD') echo ' selected'; ?>>Sudan</option>
	<option value="SR"<?php if ($row["paypalpro_default_country"] == 'SR') echo ' selected'; ?>>Suriname</option>
	<option value="SJ"<?php if ($row["paypalpro_default_country"] == 'SJ') echo ' selected'; ?>>Svalbard and Jan Mayen</option>
	<option value="SZ"<?php if ($row["paypalpro_default_country"] == 'SZ') echo ' selected'; ?>>Swaziland</option>
	<option value="SE"<?php if ($row["paypalpro_default_country"] == 'SE') echo ' selected'; ?>>Sweden</option>
	<option value="CH"<?php if ($row["paypalpro_default_country"] == 'CH') echo ' selected'; ?>>Switzerland</option>
	<option value="SY"<?php if ($row["paypalpro_default_country"] == 'SY') echo ' selected'; ?>>Syrian Arab Republic</option>
	<option value="TW"<?php if ($row["paypalpro_default_country"] == 'TW') echo ' selected'; ?>>Taiwan, Province of China</option>
	<option value="TJ"<?php if ($row["paypalpro_default_country"] == 'TJ') echo ' selected'; ?>>Tajikistan</option>
	<option value="TZ"<?php if ($row["paypalpro_default_country"] == 'TZ') echo ' selected'; ?>>Tanzania, United Republic of</option>
	<option value="TH"<?php if ($row["paypalpro_default_country"] == 'TH') echo ' selected'; ?>>Thailand</option>
	<option value="TL"<?php if ($row["paypalpro_default_country"] == 'TL') echo ' selected'; ?>>Timor-Leste</option>
	<option value="TG"<?php if ($row["paypalpro_default_country"] == 'TG') echo ' selected'; ?>>Togo</option>
	<option value="TK"<?php if ($row["paypalpro_default_country"] == 'TK') echo ' selected'; ?>>Tokelau</option>
	<option value="TO"<?php if ($row["paypalpro_default_country"] == 'TO') echo ' selected'; ?>>Tonga</option>
	<option value="TT"<?php if ($row["paypalpro_default_country"] == 'TT') echo ' selected'; ?>>Trinidad and Tobago</option>
	<option value="TN"<?php if ($row["paypalpro_default_country"] == 'TN') echo ' selected'; ?>>Tunisia</option>
	<option value="TR"<?php if ($row["paypalpro_default_country"] == 'TR') echo ' selected'; ?>>Turkey</option>
	<option value="TM"<?php if ($row["paypalpro_default_country"] == 'TM') echo ' selected'; ?>>Turkmenistan</option>
	<option value="TC"<?php if ($row["paypalpro_default_country"] == 'TC') echo ' selected'; ?>>Turks and Caicos Islands</option>
	<option value="TV"<?php if ($row["paypalpro_default_country"] == 'TV') echo ' selected'; ?>>Tuvalu</option>
	<option value="UG"<?php if ($row["paypalpro_default_country"] == 'UG') echo ' selected'; ?>>Uganda</option>
	<option value="UA"<?php if ($row["paypalpro_default_country"] == 'UA') echo ' selected'; ?>>Ukraine</option>
	<option value="AE"<?php if ($row["paypalpro_default_country"] == 'AE') echo ' selected'; ?>>United Arab Emirates</option>
	<option value="GB"<?php if ($row["paypalpro_default_country"] == 'GB') echo ' selected'; ?>>United Kingdom</option>
	<option value="US"<?php if ($row["paypalpro_default_country"] == 'US') echo ' selected'; ?>>United States</option>
	<option value="UM"<?php if ($row["paypalpro_default_country"] == 'UM') echo ' selected'; ?>>United States Minor Outlying Islands</option>
	<option value="UY"<?php if ($row["paypalpro_default_country"] == 'UY') echo ' selected'; ?>>Uruguay</option>
	<option value="UZ"<?php if ($row["paypalpro_default_country"] == 'UZ') echo ' selected'; ?>>Uzbekistan</option>
	<option value="VU"<?php if ($row["paypalpro_default_country"] == 'VU') echo ' selected'; ?>>Vanuatu</option>
	<option value="VE"<?php if ($row["paypalpro_default_country"] == 'VE') echo ' selected'; ?>>Venezuela, Bolivarian Republic of</option>
	<option value="VN"<?php if ($row["paypalpro_default_country"] == 'VN') echo ' selected'; ?>>Viet Nam</option>
	<option value="VG"<?php if ($row["paypalpro_default_country"] == 'VG') echo ' selected'; ?>>Virgin Islands, British</option>
	<option value="VI"<?php if ($row["paypalpro_default_country"] == 'VI') echo ' selected'; ?>>Virgin Islands, U.S.</option>
	<option value="WF"<?php if ($row["paypalpro_default_country"] == 'WF') echo ' selected'; ?>>Wallis and Futuna</option>
	<option value="EH"<?php if ($row["paypalpro_default_country"] == 'EH') echo ' selected'; ?>>Western Sahara</option>
	<option value="YE"<?php if ($row["paypalpro_default_country"] == 'YE') echo ' selected'; ?>>Yemen</option>
	<option value="ZM"<?php if ($row["paypalpro_default_country"] == 'ZM') echo ' selected'; ?>>Zambia</option>
	<option value="ZW"<?php if ($row["paypalpro_default_country"] == 'ZW') echo ' selected'; ?>>Zimbabwe</option>
                        </select>
                    </td>
                    </tr>
                   </table>
				</div>
			</div>
			<?php
		} // end get_addon_form_settings



		/************************ ADDON CODE *****************************/

        /************************ ATTRIBUTES *****************************/

        private $form_table = 'cp_calculated_fields_form_paypalpro';
        private $_inserted = false;
		private $_cpcff_main;

        /************************ CONSTRUCT *****************************/

        function __construct()
        {
			$this->_cpcff_main = CPCFF_MAIN::instance();
			$this->description = __("The add-on adds support for PayPal Payment Pro payments to accept credit cars directly into the website", 'calculated-fields-form' );
            // Check if the plugin is active
			if( !$this->addon_is_active() ) return;

			add_action( 'cpcff_process_data_before_insert', array( &$this, 'pp_payments_pro' ), 10, 3 );

			add_action( 'cpcff_process_data', array( &$this, 'pp_payments_pro_update_status' ), 10, 1 );

			add_action( 'cpcff_script_after_validation', array( &$this, 'pp_payments_script' ), 10, 2 );

			add_filter( 'cpcff_the_form', array( &$this, 'insert_payment_fields'), 99, 2 );

			if( is_admin() )
			{
				// Delete forms
				add_action( 'cpcff_delete_form', array(&$this, 'delete_form') );

				// Clone forms
				add_action( 'cpcff_clone_form', array(&$this, 'clone_form'), 10, 2 );

				// Export addon data
				add_action( 'cpcff_export_addons', array(&$this, 'export_form'), 10, 2 );

				// Import addon data
				add_action( 'cpcff_import_addons', array(&$this, 'import_form'), 10, 2 );
			}


        } // End __construct



        /************************ PRIVATE METHODS *****************************/

		/**
         * Create the database tables
         */
        protected function update_database()
		{
			global $wpdb;
			$charset_collate = $wpdb->get_charset_collate();
			$sql = "CREATE TABLE IF NOT EXISTS ".$wpdb->prefix.$this->form_table." (
					id mediumint(9) NOT NULL AUTO_INCREMENT,
					formid INT NOT NULL,
					enabled varchar(10) DEFAULT '0' NOT NULL ,
					paypalpro_api_username varchar(255) DEFAULT '' NOT NULL ,
					paypalpro_api_password varchar(255) DEFAULT '' NOT NULL ,
					paypalpro_api_signature varchar(255) DEFAULT '' NOT NULL ,
                    paypalpro_api_bperiod varchar(255) DEFAULT '' NOT NULL ,
                    paypalpro_default_country varchar(255) DEFAULT '' NOT NULL ,
					paypal_mode varchar(255) DEFAULT '' NOT NULL ,
					currency varchar(255) DEFAULT '' NOT NULL ,
					UNIQUE KEY id (id)
				) $charset_collate;";

			$wpdb->query($sql);
            $this->add_field_verify('paypalpro_api_bperiod');
            $this->add_field_verify('paypalpro_default_country');
		} // end update_database


		/**
         * connection to process payment
         */
		private function pp_payments_pro_POST($methodName_, $nvpStr_, $params)
		{

	        // Set up your API credentials, PayPal end point, and API version.
	        $API_UserName = urlencode($params->paypalpro_api_username);
	        $API_Password = urlencode($params->paypalpro_api_password);
	        $API_Signature = urlencode($params->paypalpro_api_signature);

            if ($params->paypal_mode == "sandbox")
                $API_Endpoint = "https://api-3t.sandbox.paypal.com/nvp";
            else
                $API_Endpoint = "https://api-3t.paypal.com/nvp";
	        $version = urlencode('51.0');

	        // Set the curl parameters.
	        $ch = curl_init();
	        curl_setopt($ch, CURLOPT_URL, $API_Endpoint);
	        curl_setopt($ch, CURLOPT_VERBOSE, 1);

	        // Turn off the server and peer verification (TrustManager Concept).
	        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
	        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

	        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	        curl_setopt($ch, CURLOPT_POST, 1);

	        // Set the API operation, version, and API signature in the request.
	        $nvpreq = "METHOD=$methodName_&VERSION=$version&PWD=$API_Password&USER=$API_UserName&SIGNATURE=$API_Signature$nvpStr_";

	        // Set the request as a POST FIELD for curl.
	        curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);

	        // Get response from the server.
	        $httpResponse = curl_exec($ch);

	        if(!$httpResponse) {
	        	exit("$methodName_ failed: ".curl_error($ch).'('.curl_errno($ch).')');
	        }

	        // Extract the response details.
	        $httpResponseAr = explode("&", $httpResponse);

	        $httpParsedResponseAr = array();
	        foreach ($httpResponseAr as $i => $value) {
	        	$tmpAr = explode("=", $value);
	        	if(sizeof($tmpAr) > 1) {
	        		$httpParsedResponseAr[$tmpAr[0]] = $tmpAr[1];
	        	}
	        }

	        if((0 == sizeof($httpParsedResponseAr)) || !array_key_exists('ACK', $httpParsedResponseAr)) {
	        	exit("Invalid HTTP Response for POST request($nvpreq) to $API_Endpoint.");
	        }

	        return $httpParsedResponseAr;
        } // end pp_payments_pro_POST


		/**
         * public payment fields
         */
        private function get_payment_fields ($id, $row)
        {
            ob_start();
            $checkscript = '<script type="text/javascript">function cffaddonpppro'.$id.'(){ try { if(document.getElementById("cffaddonidpaypro'.$id.'").checked) document.getElementById("opfield'.$this->addonID.$id.'").style.display=""; else document.getElementById("opfield'.$this->addonID.$id.'").style.display="none"; } catch (e) {} }setInterval("cffaddonpppro'.$id.'()",200);</script>';
            echo $checkscript;
            if (empty($row->paypalpro_default_country))
                $paypalpro_default_country = 'GB';
            else
                $paypalpro_default_country = $row->paypalpro_default_country;
?>
<div id="opfield<?php echo $this->addonID.$id; ?>">
<input type="hidden" id="cp_contact_form_paypal_paymentspro<?php echo $id; ?>" name="cp_contact_form_paypal_paymentspro<?php echo $id; ?>" value="1" />
<div id="pprol" style="width:100%;">
  <br />
  <table  cellpadding="0" cellspacing="0" style="margin:0px;border:0px;width:100%;">
   <tr style="border:0px;">
    <td style="padding:0px;border:0px;">
      <div class="fields" id="field-c0-ppp">
         <label><?php echo __('First Name','calculated-fields-form'); ?>:</label>
         <div class="dfield">
           <input type="text" size="15" name="cfpp_customer_first_name" id="cfpp_customer_first_name" value="" />
         </div>
         <div class="clearer"></div>
      </div><div class="dfield">
   </td>
    <td style="border:0px;">
      <div class="fields" id="field-c0-ppp">
         <label><?php echo __('Last Name','calculated-fields-form'); ?>:</label>
         <div class="dfield">
           <input type="text" size="15" name="cfpp_customer_last_name" id="cfpp_customer_last_name" value="" />
         </div>
         <div class="clearer"></div>
      </div>
     </td>
   </tr>
  </table>
  <table  cellpadding="0" cellspacing="0" style="margin:0px;border:0px;width:100%;">
   <tr>
    <td style="padding:0px; border:0px;" nowrap>
      <div class="fields" id="field-c0-ppp">
         <label><?php echo __('Credit Card Number','calculated-fields-form'); ?>:</label>
         <div class="dfield">
           <input type="text" size="18" name="cfpp_customer_credit_card_number" id="cfpp_customer_credit_card_number" value="" />
         </div>
         <div class="clearer"></div>
      </div>
    </td>
    <td style="border:0px;" nowrap>
      <div class="fields" id="field-c0-ppp">
         <label><?php echo __('CVV Number','calculated-fields-form'); ?>:</label>
         <div class="dfield">
           <input type="text" size="5" name="cfpp_cc_cvv2_number" id="cfpp_cc_cvv2_number" value="" />
         </div>
         <div class="clearer"></div>
      </div>
    </td>
   </tr>
  </table>
  <table  cellpadding="0" cellspacing="0" style="margin:0px;border:0px;width:100%;">
   <tr>
    <td style="padding:0px;border:0px;" nowrap>
      <div class="fields" id="field-c0-ppp">
         <label><?php echo __('Card Type','calculated-fields-form'); ?>:</label>
         <div class="dfield">
           <select name="cfpp_customer_credit_card_type" id="cfpp_customer_credit_card_type"><option value="Visa">Visa</option><option value="MasterCard">MasterCard</option><option value="Discover">Discover</option><option value="Amex">Amex</option></select>
         </div>
         <div class="clearer"></div>
      </div>
    </td>
    <td style="border:0px;" nowrap>
      <div class="fields" id="field-c0-ppp">
         <label><?php echo __('Expiration','calculated-fields-form'); ?>:</label>
         <div class="dfield">
           <select name="cfpp_cc_expiration_month">
           <option value="01">January</option>
           <option value="02">February</option>
           <option value="03">March</option>
           <option value="04">April</option>
           <option value="05">May</option>
           <option value="06">June</option>
           <option value="07">July</option>
           <option value="08">August</option>
           <option value="09">September</option>
           <option value="10">October</option>
           <option value="11">November</option>
           <option value="12">December</option>
          </select> /
          <select name="cfpp_cc_expiration_year">
          <?php $d= intval(date("Y")); for($i=$d;$i<$d+10;$i++) echo '<option value="'.$i.'">'.$i.'</option>'; ?>
          </select>
         </div>
         <div class="clearer"></div>
      </div>
   </td>
  </tr>
 </table>
  <table cellpadding="0" cellspacing="0" style="margin:0px;border:0px;width:100%;">
   <tr>
    <td style="padding:0px;border:0px;" colspan="3" nowrap><?php echo __('Address','calculated-fields-form'); ?>:<br /><div class="dfield"><input type="text" size="30" name="cfpp_customer_address1" id="cfpp_customer_address1" value="" /><br /><input type="text" size="30" name="cfpp_customer_address2" id="cfpp_customer_address2" value="" /></div></td>
   </tr>
   <tr>
    <td style="padding:0px;border:0px;" nowrap>
      <div class="fields" id="field-c0-ppp">
         <label><?php echo __('City','calculated-fields-form'); ?>:</label>
         <div class="dfield">
           <input type="text" size="15" name="cfpp_customer_city" id="cfpp_customer_city" value="" />
         </div>
         <div class="clearer"></div>
      </div>
    </td>
    <td style="border:0px;" nowrap>
      <div class="fields" id="field-c0-ppp">
         <label><?php echo __('County','calculated-fields-form'); ?>:</label>
         <div class="dfield">
           <input type="text" size="15" name="cfpp_customer_state" id="cfpp_customer_state" value="" />
         </div>
         <div class="clearer"></div>
      </div>
    </td>
    <td style="border:0px;" nowrap>
      <div class="fields" id="field-c0-ppp">
         <label><?php echo __('Post Code','calculated-fields-form'); ?>:</label>
         <div class="dfield">
           <input type="text" size="5" name="cfpp_customer_zip" id="cfpp_customer_zip" value="" />
         </div>
         <div class="clearer"></div>
      </div>
    </td>
   </tr>
   <tr>
    <td style="padding:0px;border:0px;" colspan="3" nowrap>

      <div class="fields" id="field-c0-ppp">
         <label><?php echo __('Country','calculated-fields-form'); ?>:</label>
         <div class="dfield">
<select name="cfpp_customer_country" id="cfpp_customer_country">
	<option value="AF"<?php if ($paypalpro_default_country == 'AF') echo ' selected'; ?>>Afghanistan</option>
	<option value="AX"<?php if ($paypalpro_default_country == 'AX') echo ' selected'; ?>>�land Islands</option>
	<option value="AL"<?php if ($paypalpro_default_country == 'AL') echo ' selected'; ?>>Albania</option>
	<option value="DZ"<?php if ($paypalpro_default_country == 'DZ') echo ' selected'; ?>>Algeria</option>
	<option value="AS"<?php if ($paypalpro_default_country == 'AS') echo ' selected'; ?>>American Samoa</option>
	<option value="AD"<?php if ($paypalpro_default_country == 'AD') echo ' selected'; ?>>Andorra</option>
	<option value="AO"<?php if ($paypalpro_default_country == 'AO') echo ' selected'; ?>>Angola</option>
	<option value="AI"<?php if ($paypalpro_default_country == 'AI') echo ' selected'; ?>>Anguilla</option>
	<option value="AQ"<?php if ($paypalpro_default_country == 'AQ') echo ' selected'; ?>>Antarctica</option>
	<option value="AG"<?php if ($paypalpro_default_country == 'AG') echo ' selected'; ?>>Antigua and Barbuda</option>
	<option value="AR"<?php if ($paypalpro_default_country == 'AR') echo ' selected'; ?>>Argentina</option>
	<option value="AM"<?php if ($paypalpro_default_country == 'AM') echo ' selected'; ?>>Armenia</option>
	<option value="AW"<?php if ($paypalpro_default_country == 'AW') echo ' selected'; ?>>Aruba</option>
	<option value="AU"<?php if ($paypalpro_default_country == 'AU') echo ' selected'; ?>>Australia</option>
	<option value="AT"<?php if ($paypalpro_default_country == 'AT') echo ' selected'; ?>>Austria</option>
	<option value="AZ"<?php if ($paypalpro_default_country == 'AZ') echo ' selected'; ?>>Azerbaijan</option>
	<option value="BS"<?php if ($paypalpro_default_country == 'BS') echo ' selected'; ?>>Bahamas</option>
	<option value="BH"<?php if ($paypalpro_default_country == 'BH') echo ' selected'; ?>>Bahrain</option>
	<option value="BD"<?php if ($paypalpro_default_country == 'BD') echo ' selected'; ?>>Bangladesh</option>
	<option value="BB"<?php if ($paypalpro_default_country == 'BB') echo ' selected'; ?>>Barbados</option>
	<option value="BY"<?php if ($paypalpro_default_country == 'BY') echo ' selected'; ?>>Belarus</option>
	<option value="BE"<?php if ($paypalpro_default_country == 'BE') echo ' selected'; ?>>Belgium</option>
	<option value="BZ"<?php if ($paypalpro_default_country == 'BZ') echo ' selected'; ?>>Belize</option>
	<option value="BJ"<?php if ($paypalpro_default_country == 'BJ') echo ' selected'; ?>>Benin</option>
	<option value="BM"<?php if ($paypalpro_default_country == 'BM') echo ' selected'; ?>>Bermuda</option>
	<option value="BT"<?php if ($paypalpro_default_country == 'BT') echo ' selected'; ?>>Bhutan</option>
	<option value="BO"<?php if ($paypalpro_default_country == 'BO') echo ' selected'; ?>>Bolivia, Plurinational State of</option>
	<option value="BQ"<?php if ($paypalpro_default_country == 'BQ') echo ' selected'; ?>>Bonaire, Sint Eustatius and Saba</option>
	<option value="BA"<?php if ($paypalpro_default_country == 'BA') echo ' selected'; ?>>Bosnia and Herzegovina</option>
	<option value="BW"<?php if ($paypalpro_default_country == 'BW') echo ' selected'; ?>>Botswana</option>
	<option value="BV"<?php if ($paypalpro_default_country == 'BV') echo ' selected'; ?>>Bouvet Island</option>
	<option value="BR"<?php if ($paypalpro_default_country == 'BR') echo ' selected'; ?>>Brazil</option>
	<option value="IO"<?php if ($paypalpro_default_country == 'IO') echo ' selected'; ?>>British Indian Ocean Territory</option>
	<option value="BN"<?php if ($paypalpro_default_country == 'BN') echo ' selected'; ?>>Brunei Darussalam</option>
	<option value="BG"<?php if ($paypalpro_default_country == 'BG') echo ' selected'; ?>>Bulgaria</option>
	<option value="BF"<?php if ($paypalpro_default_country == 'BF') echo ' selected'; ?>>Burkina Faso</option>
	<option value="BI"<?php if ($paypalpro_default_country == 'BI') echo ' selected'; ?>>Burundi</option>
	<option value="KH"<?php if ($paypalpro_default_country == 'KH') echo ' selected'; ?>>Cambodia</option>
	<option value="CM"<?php if ($paypalpro_default_country == 'CM') echo ' selected'; ?>>Cameroon</option>
	<option value="CA"<?php if ($paypalpro_default_country == 'CA') echo ' selected'; ?>>Canada</option>
	<option value="CV"<?php if ($paypalpro_default_country == 'CV') echo ' selected'; ?>>Cape Verde</option>
	<option value="KY"<?php if ($paypalpro_default_country == 'KY') echo ' selected'; ?>>Cayman Islands</option>
	<option value="CF"<?php if ($paypalpro_default_country == 'CF') echo ' selected'; ?>>Central African Republic</option>
	<option value="TD"<?php if ($paypalpro_default_country == 'TD') echo ' selected'; ?>>Chad</option>
	<option value="CL"<?php if ($paypalpro_default_country == 'CL') echo ' selected'; ?>>Chile</option>
	<option value="CN"<?php if ($paypalpro_default_country == 'CN') echo ' selected'; ?>>China</option>
	<option value="CX"<?php if ($paypalpro_default_country == 'CX') echo ' selected'; ?>>Christmas Island</option>
	<option value="CC"<?php if ($paypalpro_default_country == 'CC') echo ' selected'; ?>>Cocos (Keeling) Islands</option>
	<option value="CO"<?php if ($paypalpro_default_country == 'CO') echo ' selected'; ?>>Colombia</option>
	<option value="KM"<?php if ($paypalpro_default_country == 'KM') echo ' selected'; ?>>Comoros</option>
	<option value="CG"<?php if ($paypalpro_default_country == 'CG') echo ' selected'; ?>>Congo</option>
	<option value="CD"<?php if ($paypalpro_default_country == 'CD') echo ' selected'; ?>>Congo, the Democratic Republic of the</option>
	<option value="CK"<?php if ($paypalpro_default_country == 'CK') echo ' selected'; ?>>Cook Islands</option>
	<option value="CR"<?php if ($paypalpro_default_country == 'CR') echo ' selected'; ?>>Costa Rica</option>
	<option value="CI"<?php if ($paypalpro_default_country == 'CI') echo ' selected'; ?>>C�te d'Ivoire</option>
	<option value="HR"<?php if ($paypalpro_default_country == 'HR') echo ' selected'; ?>>Croatia</option>
	<option value="CU"<?php if ($paypalpro_default_country == 'CU') echo ' selected'; ?>>Cuba</option>
	<option value="CW"<?php if ($paypalpro_default_country == 'CW') echo ' selected'; ?>>Cura�ao</option>
	<option value="CY"<?php if ($paypalpro_default_country == 'CY') echo ' selected'; ?>>Cyprus</option>
	<option value="CZ"<?php if ($paypalpro_default_country == 'CZ') echo ' selected'; ?>>Czech Republic</option>
	<option value="DK"<?php if ($paypalpro_default_country == 'DK') echo ' selected'; ?>>Denmark</option>
	<option value="DJ"<?php if ($paypalpro_default_country == 'DJ') echo ' selected'; ?>>Djibouti</option>
	<option value="DM"<?php if ($paypalpro_default_country == 'DM') echo ' selected'; ?>>Dominica</option>
	<option value="DO"<?php if ($paypalpro_default_country == 'DO') echo ' selected'; ?>>Dominican Republic</option>
	<option value="EC"<?php if ($paypalpro_default_country == 'EC') echo ' selected'; ?>>Ecuador</option>
	<option value="EG"<?php if ($paypalpro_default_country == 'EG') echo ' selected'; ?>>Egypt</option>
	<option value="SV"<?php if ($paypalpro_default_country == 'SV') echo ' selected'; ?>>El Salvador</option>
	<option value="GQ"<?php if ($paypalpro_default_country == 'GQ') echo ' selected'; ?>>Equatorial Guinea</option>
	<option value="ER"<?php if ($paypalpro_default_country == 'ER') echo ' selected'; ?>>Eritrea</option>
	<option value="EE"<?php if ($paypalpro_default_country == 'EE') echo ' selected'; ?>>Estonia</option>
	<option value="ET"<?php if ($paypalpro_default_country == 'ET') echo ' selected'; ?>>Ethiopia</option>
	<option value="FK"<?php if ($paypalpro_default_country == 'FK') echo ' selected'; ?>>Falkland Islands (Malvinas)</option>
	<option value="FO"<?php if ($paypalpro_default_country == 'FO') echo ' selected'; ?>>Faroe Islands</option>
	<option value="FJ"<?php if ($paypalpro_default_country == 'FJ') echo ' selected'; ?>>Fiji</option>
	<option value="FI"<?php if ($paypalpro_default_country == 'FI') echo ' selected'; ?>>Finland</option>
	<option value="FR"<?php if ($paypalpro_default_country == 'FR') echo ' selected'; ?>>France</option>
	<option value="GF"<?php if ($paypalpro_default_country == 'GF') echo ' selected'; ?>>French Guiana</option>
	<option value="PF"<?php if ($paypalpro_default_country == 'PF') echo ' selected'; ?>>French Polynesia</option>
	<option value="TF"<?php if ($paypalpro_default_country == 'TF') echo ' selected'; ?>>French Southern Territories</option>
	<option value="GA"<?php if ($paypalpro_default_country == 'GA') echo ' selected'; ?>>Gabon</option>
	<option value="GM"<?php if ($paypalpro_default_country == 'GM') echo ' selected'; ?>>Gambia</option>
	<option value="GE"<?php if ($paypalpro_default_country == 'GE') echo ' selected'; ?>>Georgia</option>
	<option value="DE"<?php if ($paypalpro_default_country == 'DE') echo ' selected'; ?>>Germany</option>
	<option value="GH"<?php if ($paypalpro_default_country == 'GH') echo ' selected'; ?>>Ghana</option>
	<option value="GI"<?php if ($paypalpro_default_country == 'GI') echo ' selected'; ?>>Gibraltar</option>
	<option value="GR"<?php if ($paypalpro_default_country == 'GR') echo ' selected'; ?>>Greece</option>
	<option value="GL"<?php if ($paypalpro_default_country == 'GL') echo ' selected'; ?>>Greenland</option>
	<option value="GD"<?php if ($paypalpro_default_country == 'GD') echo ' selected'; ?>>Grenada</option>
	<option value="GP"<?php if ($paypalpro_default_country == 'GP') echo ' selected'; ?>>Guadeloupe</option>
	<option value="GU"<?php if ($paypalpro_default_country == 'GU') echo ' selected'; ?>>Guam</option>
	<option value="GT"<?php if ($paypalpro_default_country == 'GT') echo ' selected'; ?>>Guatemala</option>
	<option value="GG"<?php if ($paypalpro_default_country == 'GG') echo ' selected'; ?>>Guernsey</option>
	<option value="GN"<?php if ($paypalpro_default_country == 'GN') echo ' selected'; ?>>Guinea</option>
	<option value="GW"<?php if ($paypalpro_default_country == 'GW') echo ' selected'; ?>>Guinea-Bissau</option>
	<option value="GY"<?php if ($paypalpro_default_country == 'GY') echo ' selected'; ?>>Guyana</option>
	<option value="HT"<?php if ($paypalpro_default_country == 'HT') echo ' selected'; ?>>Haiti</option>
	<option value="HM"<?php if ($paypalpro_default_country == 'HM') echo ' selected'; ?>>Heard Island and McDonald Islands</option>
	<option value="VA"<?php if ($paypalpro_default_country == 'VA') echo ' selected'; ?>>Holy See (Vatican City State)</option>
	<option value="HN"<?php if ($paypalpro_default_country == 'HN') echo ' selected'; ?>>Honduras</option>
	<option value="HK"<?php if ($paypalpro_default_country == 'HK') echo ' selected'; ?>>Hong Kong</option>
	<option value="HU"<?php if ($paypalpro_default_country == 'HU') echo ' selected'; ?>>Hungary</option>
	<option value="IS"<?php if ($paypalpro_default_country == 'IS') echo ' selected'; ?>>Iceland</option>
	<option value="IN"<?php if ($paypalpro_default_country == 'IN') echo ' selected'; ?>>India</option>
	<option value="ID"<?php if ($paypalpro_default_country == 'ID') echo ' selected'; ?>>Indonesia</option>
	<option value="IR"<?php if ($paypalpro_default_country == 'IR') echo ' selected'; ?>>Iran, Islamic Republic of</option>
	<option value="IQ"<?php if ($paypalpro_default_country == 'IQ') echo ' selected'; ?>>Iraq</option>
	<option value="IE"<?php if ($paypalpro_default_country == 'IE') echo ' selected'; ?>>Ireland</option>
	<option value="IM"<?php if ($paypalpro_default_country == 'IM') echo ' selected'; ?>>Isle of Man</option>
	<option value="IL"<?php if ($paypalpro_default_country == 'IL') echo ' selected'; ?>>Israel</option>
	<option value="IT"<?php if ($paypalpro_default_country == 'IT') echo ' selected'; ?>>Italy</option>
	<option value="JM"<?php if ($paypalpro_default_country == 'JM') echo ' selected'; ?>>Jamaica</option>
	<option value="JP"<?php if ($paypalpro_default_country == 'JP') echo ' selected'; ?>>Japan</option>
	<option value="JE"<?php if ($paypalpro_default_country == 'JE') echo ' selected'; ?>>Jersey</option>
	<option value="JO"<?php if ($paypalpro_default_country == 'JO') echo ' selected'; ?>>Jordan</option>
	<option value="KZ"<?php if ($paypalpro_default_country == 'KZ') echo ' selected'; ?>>Kazakhstan</option>
	<option value="KE"<?php if ($paypalpro_default_country == 'KE') echo ' selected'; ?>>Kenya</option>
	<option value="KI"<?php if ($paypalpro_default_country == 'KI') echo ' selected'; ?>>Kiribati</option>
	<option value="KP"<?php if ($paypalpro_default_country == 'KP') echo ' selected'; ?>>Korea, Democratic People's Republic of</option>
	<option value="KR"<?php if ($paypalpro_default_country == 'KR') echo ' selected'; ?>>Korea, Republic of</option>
	<option value="KW"<?php if ($paypalpro_default_country == 'KW') echo ' selected'; ?>>Kuwait</option>
	<option value="KG"<?php if ($paypalpro_default_country == 'KG') echo ' selected'; ?>>Kyrgyzstan</option>
	<option value="LA"<?php if ($paypalpro_default_country == 'LA') echo ' selected'; ?>>Lao People's Democratic Republic</option>
	<option value="LV"<?php if ($paypalpro_default_country == 'LV') echo ' selected'; ?>>Latvia</option>
	<option value="LB"<?php if ($paypalpro_default_country == 'LB') echo ' selected'; ?>>Lebanon</option>
	<option value="LS"<?php if ($paypalpro_default_country == 'LS') echo ' selected'; ?>>Lesotho</option>
	<option value="LR"<?php if ($paypalpro_default_country == 'LR') echo ' selected'; ?>>Liberia</option>
	<option value="LY"<?php if ($paypalpro_default_country == 'LY') echo ' selected'; ?>>Libya</option>
	<option value="LI"<?php if ($paypalpro_default_country == 'LI') echo ' selected'; ?>>Liechtenstein</option>
	<option value="LT"<?php if ($paypalpro_default_country == 'LT') echo ' selected'; ?>>Lithuania</option>
	<option value="LU"<?php if ($paypalpro_default_country == 'LU') echo ' selected'; ?>>Luxembourg</option>
	<option value="MO"<?php if ($paypalpro_default_country == 'MO') echo ' selected'; ?>>Macao</option>
	<option value="MK"<?php if ($paypalpro_default_country == 'MK') echo ' selected'; ?>>Macedonia, the former Yugoslav Republic of</option>
	<option value="MG"<?php if ($paypalpro_default_country == 'MG') echo ' selected'; ?>>Madagascar</option>
	<option value="MW"<?php if ($paypalpro_default_country == 'MW') echo ' selected'; ?>>Malawi</option>
	<option value="MY"<?php if ($paypalpro_default_country == 'MY') echo ' selected'; ?>>Malaysia</option>
	<option value="MV"<?php if ($paypalpro_default_country == 'MV') echo ' selected'; ?>>Maldives</option>
	<option value="ML"<?php if ($paypalpro_default_country == 'ML') echo ' selected'; ?>>Mali</option>
	<option value="MT"<?php if ($paypalpro_default_country == 'MT') echo ' selected'; ?>>Malta</option>
	<option value="MH"<?php if ($paypalpro_default_country == 'MH') echo ' selected'; ?>>Marshall Islands</option>
	<option value="MQ"<?php if ($paypalpro_default_country == 'MQ') echo ' selected'; ?>>Martinique</option>
	<option value="MR"<?php if ($paypalpro_default_country == 'MR') echo ' selected'; ?>>Mauritania</option>
	<option value="MU"<?php if ($paypalpro_default_country == 'MU') echo ' selected'; ?>>Mauritius</option>
	<option value="YT"<?php if ($paypalpro_default_country == 'YT') echo ' selected'; ?>>Mayotte</option>
	<option value="MX"<?php if ($paypalpro_default_country == 'MX') echo ' selected'; ?>>Mexico</option>
	<option value="FM"<?php if ($paypalpro_default_country == 'FM') echo ' selected'; ?>>Micronesia, Federated States of</option>
	<option value="MD"<?php if ($paypalpro_default_country == 'MD') echo ' selected'; ?>>Moldova, Republic of</option>
	<option value="MC"<?php if ($paypalpro_default_country == 'MC') echo ' selected'; ?>>Monaco</option>
	<option value="MN"<?php if ($paypalpro_default_country == 'MN') echo ' selected'; ?>>Mongolia</option>
	<option value="ME"<?php if ($paypalpro_default_country == 'ME') echo ' selected'; ?>>Montenegro</option>
	<option value="MS"<?php if ($paypalpro_default_country == 'MS') echo ' selected'; ?>>Montserrat</option>
	<option value="MA"<?php if ($paypalpro_default_country == 'MA') echo ' selected'; ?>>Morocco</option>
	<option value="MZ"<?php if ($paypalpro_default_country == 'MZ') echo ' selected'; ?>>Mozambique</option>
	<option value="MM"<?php if ($paypalpro_default_country == 'MM') echo ' selected'; ?>>Myanmar</option>
	<option value="NA"<?php if ($paypalpro_default_country == 'NA') echo ' selected'; ?>>Namibia</option>
	<option value="NR"<?php if ($paypalpro_default_country == 'NR') echo ' selected'; ?>>Nauru</option>
	<option value="NP"<?php if ($paypalpro_default_country == 'NP') echo ' selected'; ?>>Nepal</option>
	<option value="NL"<?php if ($paypalpro_default_country == 'NL') echo ' selected'; ?>>Netherlands</option>
	<option value="NC"<?php if ($paypalpro_default_country == 'NC') echo ' selected'; ?>>New Caledonia</option>
	<option value="NZ"<?php if ($paypalpro_default_country == 'NZ') echo ' selected'; ?>>New Zealand</option>
	<option value="NI"<?php if ($paypalpro_default_country == 'NI') echo ' selected'; ?>>Nicaragua</option>
	<option value="NE"<?php if ($paypalpro_default_country == 'NE') echo ' selected'; ?>>Niger</option>
	<option value="NG"<?php if ($paypalpro_default_country == 'NG') echo ' selected'; ?>>Nigeria</option>
	<option value="NU"<?php if ($paypalpro_default_country == 'NU') echo ' selected'; ?>>Niue</option>
	<option value="NF"<?php if ($paypalpro_default_country == 'NF') echo ' selected'; ?>>Norfolk Island</option>
	<option value="MP"<?php if ($paypalpro_default_country == 'MP') echo ' selected'; ?>>Northern Mariana Islands</option>
	<option value="NO"<?php if ($paypalpro_default_country == 'NO') echo ' selected'; ?>>Norway</option>
	<option value="OM"<?php if ($paypalpro_default_country == 'OM') echo ' selected'; ?>>Oman</option>
	<option value="PK"<?php if ($paypalpro_default_country == 'PK') echo ' selected'; ?>>Pakistan</option>
	<option value="PW"<?php if ($paypalpro_default_country == 'PW') echo ' selected'; ?>>Palau</option>
	<option value="PS"<?php if ($paypalpro_default_country == 'PS') echo ' selected'; ?>>Palestinian Territory, Occupied</option>
	<option value="PA"<?php if ($paypalpro_default_country == 'PA') echo ' selected'; ?>>Panama</option>
	<option value="PG"<?php if ($paypalpro_default_country == 'PG') echo ' selected'; ?>>Papua New Guinea</option>
	<option value="PY"<?php if ($paypalpro_default_country == 'PY') echo ' selected'; ?>>Paraguay</option>
	<option value="PE"<?php if ($paypalpro_default_country == 'PE') echo ' selected'; ?>>Peru</option>
	<option value="PH"<?php if ($paypalpro_default_country == 'PH') echo ' selected'; ?>>Philippines</option>
	<option value="PN"<?php if ($paypalpro_default_country == 'PN') echo ' selected'; ?>>Pitcairn</option>
	<option value="PL"<?php if ($paypalpro_default_country == 'PL') echo ' selected'; ?>>Poland</option>
	<option value="PT"<?php if ($paypalpro_default_country == 'PT') echo ' selected'; ?>>Portugal</option>
	<option value="PR"<?php if ($paypalpro_default_country == 'PR') echo ' selected'; ?>>Puerto Rico</option>
	<option value="QA"<?php if ($paypalpro_default_country == 'QA') echo ' selected'; ?>>Qatar</option>
	<option value="RE"<?php if ($paypalpro_default_country == 'RE') echo ' selected'; ?>>R�union</option>
	<option value="RO"<?php if ($paypalpro_default_country == 'RO') echo ' selected'; ?>>Romania</option>
	<option value="RU"<?php if ($paypalpro_default_country == 'RU') echo ' selected'; ?>>Russian Federation</option>
	<option value="RW"<?php if ($paypalpro_default_country == 'RW') echo ' selected'; ?>>Rwanda</option>
	<option value="BL"<?php if ($paypalpro_default_country == 'BL') echo ' selected'; ?>>Saint Barth�lemy</option>
	<option value="SH"<?php if ($paypalpro_default_country == 'SH') echo ' selected'; ?>>Saint Helena, Ascension and Tristan da Cunha</option>
	<option value="KN"<?php if ($paypalpro_default_country == 'KN') echo ' selected'; ?>>Saint Kitts and Nevis</option>
	<option value="LC"<?php if ($paypalpro_default_country == 'LC') echo ' selected'; ?>>Saint Lucia</option>
	<option value="MF"<?php if ($paypalpro_default_country == 'MF') echo ' selected'; ?>>Saint Martin (French part)</option>
	<option value="PM"<?php if ($paypalpro_default_country == 'PM') echo ' selected'; ?>>Saint Pierre and Miquelon</option>
	<option value="VC"<?php if ($paypalpro_default_country == 'VC') echo ' selected'; ?>>Saint Vincent and the Grenadines</option>
	<option value="WS"<?php if ($paypalpro_default_country == 'WS') echo ' selected'; ?>>Samoa</option>
	<option value="SM"<?php if ($paypalpro_default_country == 'SM') echo ' selected'; ?>>San Marino</option>
	<option value="ST"<?php if ($paypalpro_default_country == 'ST') echo ' selected'; ?>>Sao Tome and Principe</option>
	<option value="SA"<?php if ($paypalpro_default_country == 'SA') echo ' selected'; ?>>Saudi Arabia</option>
	<option value="SN"<?php if ($paypalpro_default_country == 'SN') echo ' selected'; ?>>Senegal</option>
	<option value="RS"<?php if ($paypalpro_default_country == 'RS') echo ' selected'; ?>>Serbia</option>
	<option value="SC"<?php if ($paypalpro_default_country == 'SC') echo ' selected'; ?>>Seychelles</option>
	<option value="SL"<?php if ($paypalpro_default_country == 'SL') echo ' selected'; ?>>Sierra Leone</option>
	<option value="SG"<?php if ($paypalpro_default_country == 'SG') echo ' selected'; ?>>Singapore</option>
	<option value="SX"<?php if ($paypalpro_default_country == 'SX') echo ' selected'; ?>>Sint Maarten (Dutch part)</option>
	<option value="SK"<?php if ($paypalpro_default_country == 'SK') echo ' selected'; ?>>Slovakia</option>
	<option value="SI"<?php if ($paypalpro_default_country == 'SI') echo ' selected'; ?>>Slovenia</option>
	<option value="SB"<?php if ($paypalpro_default_country == 'SB') echo ' selected'; ?>>Solomon Islands</option>
	<option value="SO"<?php if ($paypalpro_default_country == 'SO') echo ' selected'; ?>>Somalia</option>
	<option value="ZA"<?php if ($paypalpro_default_country == 'ZA') echo ' selected'; ?>>South Africa</option>
	<option value="GS"<?php if ($paypalpro_default_country == 'GS') echo ' selected'; ?>>South Georgia and the South Sandwich Islands</option>
	<option value="SS"<?php if ($paypalpro_default_country == 'SS') echo ' selected'; ?>>South Sudan</option>
	<option value="ES"<?php if ($paypalpro_default_country == 'ES') echo ' selected'; ?>>Spain</option>
	<option value="LK"<?php if ($paypalpro_default_country == 'LK') echo ' selected'; ?>>Sri Lanka</option>
	<option value="SD"<?php if ($paypalpro_default_country == 'SD') echo ' selected'; ?>>Sudan</option>
	<option value="SR"<?php if ($paypalpro_default_country == 'SR') echo ' selected'; ?>>Suriname</option>
	<option value="SJ"<?php if ($paypalpro_default_country == 'SJ') echo ' selected'; ?>>Svalbard and Jan Mayen</option>
	<option value="SZ"<?php if ($paypalpro_default_country == 'SZ') echo ' selected'; ?>>Swaziland</option>
	<option value="SE"<?php if ($paypalpro_default_country == 'SE') echo ' selected'; ?>>Sweden</option>
	<option value="CH"<?php if ($paypalpro_default_country == 'CH') echo ' selected'; ?>>Switzerland</option>
	<option value="SY"<?php if ($paypalpro_default_country == 'SY') echo ' selected'; ?>>Syrian Arab Republic</option>
	<option value="TW"<?php if ($paypalpro_default_country == 'TW') echo ' selected'; ?>>Taiwan, Province of China</option>
	<option value="TJ"<?php if ($paypalpro_default_country == 'TJ') echo ' selected'; ?>>Tajikistan</option>
	<option value="TZ"<?php if ($paypalpro_default_country == 'TZ') echo ' selected'; ?>>Tanzania, United Republic of</option>
	<option value="TH"<?php if ($paypalpro_default_country == 'TH') echo ' selected'; ?>>Thailand</option>
	<option value="TL"<?php if ($paypalpro_default_country == 'TL') echo ' selected'; ?>>Timor-Leste</option>
	<option value="TG"<?php if ($paypalpro_default_country == 'TG') echo ' selected'; ?>>Togo</option>
	<option value="TK"<?php if ($paypalpro_default_country == 'TK') echo ' selected'; ?>>Tokelau</option>
	<option value="TO"<?php if ($paypalpro_default_country == 'TO') echo ' selected'; ?>>Tonga</option>
	<option value="TT"<?php if ($paypalpro_default_country == 'TT') echo ' selected'; ?>>Trinidad and Tobago</option>
	<option value="TN"<?php if ($paypalpro_default_country == 'TN') echo ' selected'; ?>>Tunisia</option>
	<option value="TR"<?php if ($paypalpro_default_country == 'TR') echo ' selected'; ?>>Turkey</option>
	<option value="TM"<?php if ($paypalpro_default_country == 'TM') echo ' selected'; ?>>Turkmenistan</option>
	<option value="TC"<?php if ($paypalpro_default_country == 'TC') echo ' selected'; ?>>Turks and Caicos Islands</option>
	<option value="TV"<?php if ($paypalpro_default_country == 'TV') echo ' selected'; ?>>Tuvalu</option>
	<option value="UG"<?php if ($paypalpro_default_country == 'UG') echo ' selected'; ?>>Uganda</option>
	<option value="UA"<?php if ($paypalpro_default_country == 'UA') echo ' selected'; ?>>Ukraine</option>
	<option value="AE"<?php if ($paypalpro_default_country == 'AE') echo ' selected'; ?>>United Arab Emirates</option>
	<option value="GB"<?php if ($paypalpro_default_country == 'GB') echo ' selected'; ?>>United Kingdom</option>
	<option value="US"<?php if ($paypalpro_default_country == 'US') echo ' selected'; ?>>United States</option>
	<option value="UM"<?php if ($paypalpro_default_country == 'UM') echo ' selected'; ?>>United States Minor Outlying Islands</option>
	<option value="UY"<?php if ($paypalpro_default_country == 'UY') echo ' selected'; ?>>Uruguay</option>
	<option value="UZ"<?php if ($paypalpro_default_country == 'UZ') echo ' selected'; ?>>Uzbekistan</option>
	<option value="VU"<?php if ($paypalpro_default_country == 'VU') echo ' selected'; ?>>Vanuatu</option>
	<option value="VE"<?php if ($paypalpro_default_country == 'VE') echo ' selected'; ?>>Venezuela, Bolivarian Republic of</option>
	<option value="VN"<?php if ($paypalpro_default_country == 'VN') echo ' selected'; ?>>Viet Nam</option>
	<option value="VG"<?php if ($paypalpro_default_country == 'VG') echo ' selected'; ?>>Virgin Islands, British</option>
	<option value="VI"<?php if ($paypalpro_default_country == 'VI') echo ' selected'; ?>>Virgin Islands, U.S.</option>
	<option value="WF"<?php if ($paypalpro_default_country == 'WF') echo ' selected'; ?>>Wallis and Futuna</option>
	<option value="EH"<?php if ($paypalpro_default_country == 'EH') echo ' selected'; ?>>Western Sahara</option>
	<option value="YE"<?php if ($paypalpro_default_country == 'YE') echo ' selected'; ?>>Yemen</option>
	<option value="ZM"<?php if ($paypalpro_default_country == 'ZM') echo ' selected'; ?>>Zambia</option>
	<option value="ZW"<?php if ($paypalpro_default_country == 'ZW') echo ' selected'; ?>>Zimbabwe</option>

</select>
         </div>
         <div class="clearer"></div>
      </div>
    </td>
   </tr>
  </table>
</div>
</div>
<?php
            $buffered_contents = ob_get_contents();
            ob_end_clean();
            return $buffered_contents;
        }


		/************************ PUBLIC METHODS  *****************************/


	    /**
         * Check if the payments fields is used in the form, and inserts them
         */
        public function	insert_payment_fields( $form_code, $id)
		{
			global $wpdb;
			$rows = $wpdb->get_results(
					$wpdb->prepare( "SELECT * FROM ".$wpdb->prefix.$this->form_table." WHERE formid=%d", $id )
				);
			if ( !empty( $rows ) && $rows[0]->enabled)
			{
			    $this->_inserted = true;
			    $form_code = preg_replace( '/<!--addons-payment-fields-->/i', '<!--addons-payment-fields-->'.$this->get_payment_fields($id, $rows[0]), $form_code );

			    // output radio-buttons here
    			$form_code = preg_replace( '/<!--addons-payment-options-->/i', '<div><input type="radio" id="cffaddonidpaypro'.$id.'" name="bccf_payment_option_paypal" vt="'.$this->addonID.'" value="'.$this->addonID.'" checked> '.__('Pay with Credit Cards', 'calculated-fields-form').'</div><!--addons-payment-options-->', $form_code );

                if (($rows[0]->enabled == '2' || $rows[0]->enabled == '4') && !strpos($form_code,'bccf_payment_option_paypal" vt="0') )
    			    $form_code = preg_replace( '/<!--addons-payment-options-->/i', '<!--addons-payment-options--><div><input type="radio" name="bccf_payment_option_paypal" vt="0" value="0"> '.__( $this->_cpcff_main->get_form($id)->get_option('enable_paypal_option_no',CP_CALCULATEDFIELDSF_PAYPAL_OPTION_NO), 'calculated-fields-form').'</div>', $form_code );

    			if (substr_count ($form_code, 'name="bccf_payment_option_paypal"') > 1)
    			    $form_code = str_replace( 'id="field-c0" style="display:none">', 'id="field-c0">', $form_code);
			}

            return $form_code;
		} // End insert_script


		/**
         * script process payment
         */
		public function pp_payments_script( $form_sequence_id, $form_id )
		{
            global $wpdb;

            $rows = $wpdb->get_results(
						$wpdb->prepare( "SELECT * FROM ".$wpdb->prefix.$this->form_table." WHERE formid=%d", $form_id )
					);

			if (empty( $rows ) || !$rows[0]->enabled)
			    return;
?>
			if(
				document.getElementById("cffaddonidpaypro<?php echo $form_id; ?>").checked &&
				(
					typeof validation_rules['<?php print esc_js( $this->addonID); ?>'] == 'undefined'||
					validation_rules['<?php print esc_js( $this->addonID); ?>'] == false
				)
			)
			{
				validation_rules['<?php print esc_js( $this->addonID); ?>'] = false;
                /**
				<?php // This code was commented because cloning the form breaks the captcha ?>
				var cp_calculatedfieldsf_pform_copy = $dexQuery("#cp_calculatedfieldsf_pform<?php echo $form_sequence_id; ?>").clone(true);
				cp_calculatedfieldsf_pform_copy.find( '.ignore' ).closest( '.fields' ).remove();
				cp_calculatedfieldsf_pform_copy.find('[name="cfpp_customer_country"]').val( $dexQuery("#cp_calculatedfieldsf_pform<?php echo $form_sequence_id; ?>").find('[name="cfpp_customer_country"]').val());
				cp_calculatedfieldsf_pform_copy.find('[name="cfpp_cc_expiration_month"]').val( $dexQuery("#cp_calculatedfieldsf_pform<?php echo $form_sequence_id; ?>").find('[name="cfpp_cc_expiration_month"]').val());
				cp_calculatedfieldsf_pform_copy.find('[name="cfpp_cc_expiration_year"]').val( $dexQuery("#cp_calculatedfieldsf_pform<?php echo $form_sequence_id; ?>").find('[name="cfpp_cc_expiration_year"]').val());
				cp_calculatedfieldsf_pform_copy.find('[name="cfpp_customer_credit_card_type"]').val( $dexQuery("#cp_calculatedfieldsf_pform<?php echo $form_sequence_id; ?>").find('[name="cfpp_customer_credit_card_type"]').val());
                */
                var ppdata = $dexQuery("#cp_calculatedfieldsf_pform<?php echo $form_sequence_id; ?>").serialize() +'&'+$dexQuery.param({ 'cffpproprocess': '1' });
                $dexQuery.ajax({
                    type: "POST",
                    async: true,
                    url: '<?php echo CPCFF_AUXILIARY::site_url(); ?>/',
                    data: ppdata,
                    success: function(data)
                    {
                       if (data.trim() != 'OK')
                           alert(data);
                       else
                       {
                           document.getElementById("cp_contact_form_paypal_paymentspro<?php echo $form_id; ?>").value = "";
						   validation_rules['<?php print esc_js( $this->addonID); ?>'] = true;
                           processing_form();
                       }
                    }
                });
			}
<?php
        }


		/**
         * process payment
         */
		public function pp_payments_pro(&$params, &$str, $fields)
		{
            global $wpdb;

            $payment_option = (isset($_POST["bccf_payment_option_paypal"])?$_POST["bccf_payment_option_paypal"]:$this->addonID);
            if ($payment_option != $this->addonID)
                return;

            if (@$_POST['cp_contact_form_paypal_paymentspro'.$params["formid"]] != "1")
            {
                $params["payment_option"] = $this->name;
                return;
            }

            if (@$_POST["cffpproprocess"] != '1')
                return;

            $rows = $wpdb->get_results(
						$wpdb->prepare( "SELECT * FROM ".$wpdb->prefix.$this->form_table." WHERE formid=%d", $params["formid"] )
					);
			if (empty( $rows ) || !$rows[0]->enabled)
			    return;

            $form_obj = $this->_cpcff_main->get_form($params['formid']);

            // Set request-specific fields.
            $paymentType = urlencode('Sale');				// or 'Authorization'

            $firstName = urlencode($_POST['cfpp_customer_first_name']);
            $lastName = urlencode($_POST['cfpp_customer_last_name']);
            $creditCardType = urlencode($_POST['cfpp_customer_credit_card_type']);
            $creditCardNumber = urlencode($_POST['cfpp_customer_credit_card_number']);
            $expDateMonth = $_POST['cfpp_cc_expiration_month'];
            // Month must be padded with leading zero
            $padDateMonth = urlencode(str_pad($expDateMonth, 2, '0', STR_PAD_LEFT));

            $expDateYear = urlencode($_POST['cfpp_cc_expiration_year']);
            $cvv2Number = urlencode($_POST['cfpp_cc_cvv2_number']);
            $address1 = urlencode($_POST['cfpp_customer_address1']);
            $address2 = urlencode($_POST['cfpp_customer_address2']);
            $city = urlencode($_POST['cfpp_customer_city']);
            $state = urlencode($_POST['cfpp_customer_state']);
            $zip = urlencode($_POST['cfpp_customer_zip']);
            $country = urlencode($_POST['cfpp_customer_country']);				// US or other valid country code

            $amount = urlencode($params["final_price"]);
            $currencyID = urlencode(strtoupper($rows[0]->currency));

            if ($rows[0]->paypalpro_api_bperiod == '')
            {
                // Add request-specific fields to the request string.
                $nvpStr =	"&PAYMENTACTION=$paymentType&AMT=$amount&CREDITCARDTYPE=$creditCardType&ACCT=$creditCardNumber".
                			"&EXPDATE=$padDateMonth$expDateYear&CVV2=$cvv2Number&FIRSTNAME=$firstName&LASTNAME=$lastName".
                			"&STREET=$address1&CITY=$city&STATE=$state&ZIP=$zip&COUNTRYCODE=$country&CURRENCYCODE=$currencyID&BUTTONSOURCE=NetFactorSL_SI_Custom";

                // Execute the API operation; see the PPHttpPost function above.
                $httpParsedResponseAr = $this->pp_payments_pro_POST('DoDirectPayment', $nvpStr, $rows[0]);
            }
            else
            {
                // Add request-specific fields to the request string.
                $nvpStr =	"&MAXFAILEDPAYMENTS=3&DESC=".urlencode($form_obj->get_option('paypal_product_name', CP_CALCULATEDFIELDSF_DEFAULT_PRODUCT_NAME))."&PROFILESTARTDATE=".date("Y-m-d")."T00:00:00Z&BILLINGPERIOD=".$rows[0]->paypalpro_api_bperiod."&BILLINGFREQUENCY=1&AMT=$amount&CREDITCARDTYPE=$creditCardType&ACCT=$creditCardNumber".
                			"&EXPDATE=$padDateMonth$expDateYear&CVV2=$cvv2Number&FIRSTNAME=$firstName&LASTNAME=$lastName".
                			"&STREET=$address1&CITY=$city&STATE=$state&ZIP=$zip&COUNTRYCODE=$country&CURRENCYCODE=$currencyID&BUTTONSOURCE=NetFactorSL_SI_Custom";

                // Execute the API operation; see the PPHttpPost function above.
                $httpParsedResponseAr = $this->pp_payments_pro_POST('CreateRecurringPaymentsProfile', $nvpStr, $rows[0]);
            }
            foreach ($httpParsedResponseAr as $item => $value)
                $httpParsedResponseAr[$item] = urldecode($value);
            if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"])) {
            	exit('OK');
            } else  {
            	exit("Payment failed\n\nErrorCode: " . $httpParsedResponseAr["L_ERRORCODE0"]."\nError: ". $httpParsedResponseAr["L_SHORTMESSAGE0"]."\nMessage: ". $httpParsedResponseAr["L_LONGMESSAGE0"]);
            }
		} // end pp_payments_pro


		/**
		 * mark the item as paid
		 */
		public function pp_payments_pro_update_status( $params )
		{
            global $wpdb;

            $payment_option = (isset($_POST["bccf_payment_option_paypal"])?$_POST["bccf_payment_option_paypal"]:$this->addonID);
            if($payment_option != $this->addonID)
                return;

            $row = $wpdb->get_row(
				$wpdb->prepare( "SELECT * FROM ".$wpdb->prefix.$this->form_table." WHERE formid=%d", $params["formid"] )
			);
			if(empty($row) || !$row->enabled) return;

			CPCFF_SUBMISSIONS::update($params["itemnumber"], array('paid'=>1));

			/**
			 * Action called after process the data received by PayPal.
			 * To the function is passed an array with the data collected by the form.
			 */
			do_action( 'cpcff_payment_processed', $params );

		}


		/**
		 *	Add field if not exists
		 */
        function add_field_verify ($field, $type = "varchar(255) DEFAULT '' NOT NULL")
        {
            global $wpdb;
            $results = $wpdb->get_results("SHOW columns FROM `".$wpdb->prefix.$this->form_table."` where field='".$field."'");
            if (!count($results))
            {
                $sql = "ALTER TABLE  `".$wpdb->prefix.$this->form_table."` ADD `".$field."` ".$type;
                $wpdb->query($sql);
            }
        }


		/**
		 *	Delete the form from the addon's table
		 */
        public function delete_form( $formid)
		{
			global $wpdb;
			$wpdb->delete( $wpdb->prefix.$this->form_table, array('formid' => $formid), '%d' );
		} // delete_form

		/**
		 *	Clone the form's row
		 */
		public function clone_form( $original_form_id, $new_form_id )
		{
			global $wpdb;

			$form_rows = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM ".$wpdb->prefix.$this->form_table." WHERE formid=%d", $original_form_id ), ARRAY_A);

			if(!empty($form_rows))
			{
				foreach($form_rows as $form_row)
				{
					unset($form_row["id"]);
					$form_row["formid"] = $new_form_id;
					$wpdb->insert( $wpdb->prefix.$this->form_table, $form_row);
				}
			}
		} // End clone_form

		/**
		 *	It is called when the form is exported to export the addons data too.
		 *  Receive an array with the other addons data, and the form's id for filtering.
		 */
		public function export_form($addons_array, $formid)
		{
			global $wpdb;
			$rows = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM ".$wpdb->prefix.$this->form_table." WHERE formid=%d", $formid ), ARRAY_A );
			if(!empty($rows))
			{
				$addons_array[ $this->addonID ] = array();
				foreach($rows as $row)
				{
					unset($row['id']);
					unset($row['formid']);
					$addons_array[ $this->addonID ][] = $row;
				}
			}
			return $addons_array;
		} // End export_form

		/**
		 *	It is called when the form is imported to import the addons data too.
		 *  Receive an array with all the addons data, and the new form's id.
		 */
		public function import_form($addons_array, $formid)
		{
			global $wpdb;
			if(isset($addons_array[$this->addonID]))
			{
				foreach($addons_array[$this->addonID] as $row)
				{
					if(!empty($row))
					{
						$row['formid'] = $formid;
						$wpdb->insert(
							$wpdb->prefix.$this->form_table,
							$row
						);
					}
				}
			}
		} // End import_form

    } // End Class

    // Main add-on code
    $cpcff_paypalpro_obj = new CPCFF_PayPalPro();

	// Add addon object to the objects list
	CPCFF_ADDONS::add($cpcff_paypalpro_obj);
}
?>