<?php
/*
Documentation: https://stripe.com/docs/quickstart
*/
require_once dirname( __FILE__ ).'/base.addon.php';

if( !class_exists( 'CPCFF_Stripe' ) )
{
    class CPCFF_Stripe extends CPCFF_BaseAddon
    {

        /************* ADDON SYSTEM - ATTRIBUTES AND METHODS *************/
		protected $addonID = "addon-stripe-20151212";
		protected $name = "CFF - Stripe";
		protected $description;

		public function get_addon_form_settings( $form_id )
		{
			global $wpdb;

			// Insertion in database
			if(
				isset( $_REQUEST[ 'CPCFF_Stripe_id' ] )
			)
			{
			    // verify needed fields for update
			    $this->add_field_verify("frequency");
			    $this->add_field_verify("trialdays");
			    $this->add_field_verify("planname");

                $this->add_field_verify("stripe_mode");
                $this->add_field_verify("stripe_testkey");
                $this->add_field_verify("stripe_testsecretkey");
                $this->add_field_verify("stripe_subtitle");
                $this->add_field_verify("stripe_logoimage");

			    $wpdb->delete( $wpdb->prefix.$this->form_table, array( 'formid' => $form_id ), array( '%d' ) );
				$wpdb->insert(
								$wpdb->prefix.$this->form_table,
								array(
									'formid' => $form_id,
									'stripe_key'	 => $_REQUEST["stripe_key"],
									'stripe_secretkey'	 => $_REQUEST["stripe_secretkey"],
									'enabled'	 => $_REQUEST["stripe_enabled"],
									'frequency'	 => $_REQUEST["stripe_frequency"],
									'trialdays'	 => $_REQUEST["stripe_trialdays"],
									'planname'	 => $_REQUEST["stripe_planname"],
                                    'stripe_mode'	 => $_REQUEST["stripe_mode"],
                                    'stripe_testkey'	 => $_REQUEST["stripe_testkey"],
									'stripe_testsecretkey'	 => $_REQUEST["stripe_testsecretkey"],
                                    'stripe_subtitle'	 => $_REQUEST["stripe_subtitle"],
                                    'stripe_logoimage'	 => $_REQUEST["stripe_logoimage"],
								),
								array( '%d', '%s', '%s', '%s', '%s', '%s', '%s',
                                             '%s', '%s', '%s', '%s', '%s')
							);
			}


			$rows = $wpdb->get_results(
						$wpdb->prepare( "SELECT * FROM ".$wpdb->prefix.$this->form_table." WHERE formid=%d", $form_id )
					);
			if (!count($rows))
			{
			    $row["stripe_key"] = "";
			    $row["stripe_secretkey"] = "";
			    $row["enabled"] = "0";
			    $row["frequency"] = "";
			    $row["trialdays"] = "0";
			    $row["planname"] = "";
			    $row["stripe_mode"] = "1";
			    $row["stripe_testkey"] = "";
			    $row["stripe_testsecretkey"] = "";
			    $row["stripe_subtitle"] = "";
			    $row["stripe_logoimage"] = "";
			} else {
			    $row["stripe_key"] = $rows[0]->stripe_key;
			    $row["stripe_secretkey"] = $rows[0]->stripe_secretkey;
			    $row["enabled"] = $rows[0]->enabled;
			    $row["frequency"] = $rows[0]->frequency;
			    $row["trialdays"] = $rows[0]->trialdays;
			    $row["planname"] = $rows[0]->planname;
			    $row["stripe_mode"] = $rows[0]->stripe_mode;
			    $row["stripe_testkey"] = $rows[0]->stripe_testkey;
			    $row["stripe_testsecretkey"] = $rows[0]->stripe_testsecretkey;
			    $row["stripe_subtitle"] = $rows[0]->stripe_subtitle;
			    $row["stripe_logoimage"] = $rows[0]->stripe_logoimage;
			}

			?>
			<div id="metabox_basic_settings" class="postbox" >
				<h3 class='hndle' style="padding:5px;"><span><?php print $this->name; ?></span></h3>
				<div class="inside">
				   <input type="hidden" name="CPCFF_Stripe_id" value="1" />
                   <table class="form-table">
                    <tr valign="top">
                    <th scope="row"><?php _e('Enable Stripe?', 'calculated-fields-form'); ?></th>
                    <td><select name="stripe_enabled">
                         <option value="0" <?php if (!$row["enabled"]) echo 'selected'; ?>><?php _e('No', 'calculated-fields-form'); ?></option>
                         <option value="1" <?php if ($row["enabled"]) echo 'selected'; ?>><?php _e('Yes', 'calculated-fields-form'); ?></option>
                         <option value="2" <?php if ($row["enabled"] == '2') echo 'selected'; ?>><?php _e('Optional: This payment method + Pay Later (submit without payment)', 'calculated-fields-form'); ?></option>
                         <option value="3" <?php if ($row["enabled"] == '3') echo 'selected'; ?>><?php _e('Optional: This payment method + Other payment methods (enabled)', 'calculated-fields-form'); ?></option>
                         <option value="4" <?php if ($row["enabled"] == '4') echo 'selected'; ?>><?php _e('Optional: This payment method + Other payment methods  + Pay Later ', 'calculated-fields-form'); ?></option>
                         </select>
                    </td>
                    </tr>
                    <th scope="row"><?php _e('Payment mode?', 'calculated-fields-form'); ?></th>
                    <td><select name="stripe_mode" onchange="cffstripe_changemode(this);">
                         <option value="0" <?php if ($row["stripe_mode"] == '0') echo 'selected'; ?>><?php _e('Test Mode', 'calculated-fields-form'); ?></option>
                         <option value="1" <?php if ($row["stripe_mode"] != '0') echo 'selected'; ?>><?php _e('Live/Production Mode', 'calculated-fields-form'); ?></option>
                         </select>
                    </td>
                    </tr>
                    <tr valign="top" id="cffstripe_prod1" <?php if ($row["stripe_mode"] == '0') echo 'style="display:none"'; ?>>
                    <th scope="row">Stripe.com <span style="color:green">Production</span> <a href="https://manage.stripe.com/account/apikeys" target="_blank" title="<?php _e('click to get the your stripe key', 'calculated-fields-form'); ?>"><?php _e('Publishable Key', 'calculated-fields-form'); ?></a></th>
                    <td><input type="text" name="stripe_key" size="20" value="<?php echo esc_attr($row["stripe_key"]); ?>" /></td>
                    </tr>
                    <tr valign="top" id="cffstripe_prod2" <?php if ($row["stripe_mode"] == '0') echo 'style="display:none"'; ?>>
                    <th scope="row">Stripe.com <span style="color:green">Production</span> <a href="https://manage.stripe.com/account/apikeys" target="_blank" title="<?php _e('click to get the your stripe secret key', 'calculated-fields-form'); ?>"><?php _e('Secret Key', 'calculated-fields-form'); ?></a></th>
                    <td><input type="text" name="stripe_secretkey" size="20" value="<?php echo esc_attr($row["stripe_secretkey"]); ?>" /></td>
                    </tr>
                    <tr valign="top" id="cffstripe_test1" <?php if ($row["stripe_mode"] != '0') echo 'style="display:none"'; ?>>
                    <th scope="row">Stripe.com <span style="color:red">TEST</span> <a href="https://manage.stripe.com/account/apikeys" target="_blank" title="<?php _e('click to get the your stripe key', 'calculated-fields-form'); ?>"><?php _e('Publishable Key', 'calculated-fields-form'); ?></a></th>
                    <td><input type="text" name="stripe_testkey" size="20" value="<?php echo esc_attr($row["stripe_testkey"]); ?>" /></td>
                    </tr>
                    <tr valign="top" id="cffstripe_test2" <?php if ($row["stripe_mode"] != '0') echo 'style="display:none"'; ?>>
                    <th scope="row">Stripe.com <span style="color:red">TEST</span> <a href="https://manage.stripe.com/account/apikeys" target="_blank" title="<?php _e('click to get the your stripe secret key', 'calculated-fields-form'); ?>"><?php _e('Secret Key', 'calculated-fields-form'); ?></a></th>
                    <td><input type="text" name="stripe_testsecretkey" size="20" value="<?php echo esc_attr($row["stripe_testsecretkey"]); ?>" /></td>
                    </tr>
                    <tr valign="top">
                    <th scope="row"><?php _e('Payment frequency?', 'calculated-fields-form'); ?></th>
                    <td><select name="stripe_frequency">
                         <option value="" <?php if (!$row["frequency"]) echo 'selected'; ?>><?php _e('One time payment', 'calculated-fields-form'); ?></option>
                         <option value="day" <?php if ($row["frequency"] == 'day') echo 'selected'; ?>><?php _e('Daily (subcription)', 'calculated-fields-form'); ?></option>
                         <option value="week" <?php if ($row["frequency"] == 'week') echo 'selected'; ?>><?php _e('Weekly (subscription)', 'calculated-fields-form'); ?></option>
                         <option value="month" <?php if ($row["frequency"] == 'month') echo 'selected'; ?>><?php _e('Monthly (subscription)', 'calculated-fields-form'); ?></option>
                         <option value="year" <?php if ($row["frequency"] == 'year') echo 'selected'; ?>><?php _e('Yearly (subscription)', 'calculated-fields-form'); ?></option>
                         </select>
                    </td>
                    </tr>
                    <tr valign="top">
                    <th scope="row"><?php _e('Trial period length in days for subscription payments', 'calculated-fields-form'); ?></th>
                    <td><input type="text" name="stripe_trialdays" size="50" value="<?php echo esc_attr($row["trialdays"]); ?>" /></td>
                    </tr>
                    <tr valign="top">
                    <th scope="row"><?php _e('Plan name for subscription payments', 'calculated-fields-form'); ?></th>
                    <td><input type="text" name="stripe_planname" size="20" value="<?php echo esc_attr($row["planname"]); ?>" /></td>
                    </tr>
                    <tr valign="top">
                    <th scope="row"><?php _e('Subtitle for payment panel', 'calculated-fields-form'); ?></th>
                    <td><input type="text" name="stripe_subtitle" size="20" value="<?php echo esc_attr($row["stripe_subtitle"]); ?>" /></td>
                    </tr>
                    <tr valign="top">
                    <th scope="row"><?php _e('URL of logo image', 'calculated-fields-form'); ?></th>
                    <td><input type="text" name="stripe_logoimage" size="20" value="<?php echo esc_attr($row["stripe_logoimage"]); ?>" /><br />
                    <em>* A relative or absolute URL pointing to a square image of your brand or product. The recommended minimum size is 128x128px. The supported image types are: .gif, .jpeg, and .png.</em></td>
                    </tr>
                   </table>
				</div>
			</div>
            <script type="text/javascript">
            function cffstripe_changemode(item) {
                if (item.options.selectedIndex == 0)
                {
                    document.getElementById('cffstripe_prod1').style.display = 'none';
                    document.getElementById('cffstripe_prod2').style.display = 'none';
                    document.getElementById('cffstripe_test1').style.display = '';
                    document.getElementById('cffstripe_test2').style.display = '';
                }
                else
                {
                    document.getElementById('cffstripe_prod1').style.display = '';
                    document.getElementById('cffstripe_prod2').style.display = '';
                    document.getElementById('cffstripe_test1').style.display = 'none';
                    document.getElementById('cffstripe_test2').style.display = 'none';
                }
            }
            </script>
			<?php
		} // end get_addon_form_settings



		/************************ ADDON CODE *****************************/

        /************************ ATTRIBUTES *****************************/

        private $form_table = 'cp_calculated_fields_form_stripe';
		private $table_columns;
        private $form_table_plans = 'cp_calculated_fields_form_stripeplans';
        private $form_table_customers = 'cp_calculated_fields_form_stripecustomers';
        private $_inserted = false;
		private $_cpcff_main;

        /************************ CONSTRUCT *****************************/

        function __construct()
        {
			$this->_cpcff_main = CPCFF_MAIN::instance();
			$this->description = __("The add-on adds support for Stripe payments", 'calculated-fields-form' );
            // Check if the plugin is active
			if( !$this->addon_is_active() ) return;

			add_action( 'cpcff_process_data_before_insert', array( &$this, 'pp_stripe' ), 1, 3 );

			add_action( 'cpcff_process_data', array( &$this, 'pp_stripe_redirect' ), 11, 1 );

			add_action( 'init', array( &$this, 'pp_stripe_check_price' ), 1 );

			add_action( 'cpcff_script_after_validation', array( &$this, 'pp_payments_script' ), 10, 2 );

			add_filter( 'cpcff_the_form', array( &$this, 'insert_payment_fields'), 99, 2 );

			if( is_admin() )
			{
				// Delete forms
				add_action( 'cpcff_delete_form', array(&$this, 'delete_form') );

				// Clone forms
				add_action( 'cpcff_clone_form', array(&$this, 'clone_form'), 10, 2 );

				// Export addon data
				add_action( 'cpcff_export_addons', array(&$this, 'export_form'), 10, 2 );

				// Import addon data
				add_action( 'cpcff_import_addons', array(&$this, 'import_form'), 10, 2 );
			}


        } // End __construct



        /************************ PRIVATE METHODS *****************************/

		/**
         * Create the database tables
         */
        protected function update_database()
		{
			global $wpdb;
			$sql = "CREATE TABLE IF NOT EXISTS ".$wpdb->prefix.$this->form_table." (
					id mediumint(9) NOT NULL AUTO_INCREMENT,
					formid INT NOT NULL,
					enabled varchar(10) DEFAULT '0' NOT NULL ,
					stripe_key varchar(255) DEFAULT '' NOT NULL ,
					stripe_secretkey varchar(255) DEFAULT '' NOT NULL ,
					frequency varchar(30) DEFAULT '' NOT NULL ,
					trialdays varchar(20) DEFAULT '' NOT NULL ,
					planname varchar(255) DEFAULT '' NOT NULL ,
                    stripe_mode varchar(10) DEFAULT '' NOT NULL ,
                    stripe_testkey varchar(255) DEFAULT '' NOT NULL ,
                    stripe_testsecretkey varchar(255) DEFAULT '' NOT NULL ,
                    stripe_subtitle varchar(255) DEFAULT '' NOT NULL ,
                    stripe_logoimage varchar(255) DEFAULT '' NOT NULL ,
					UNIQUE KEY id (id)
				)
				CHARACTER SET utf8
				COLLATE utf8_general_ci;";
            $wpdb->query($sql);

			$sql = "CREATE TABLE IF NOT EXISTS ".$wpdb->prefix.$this->form_table_plans." (
					id mediumint(9) NOT NULL AUTO_INCREMENT,
					planname varchar(255) DEFAULT '' NOT NULL ,
					planinterval varchar(255) DEFAULT '' NOT NULL ,
					currency varchar(30) DEFAULT '' NOT NULL ,
					amount varchar(20) DEFAULT '' NOT NULL ,
					trial_period_days varchar(20) DEFAULT '' NOT NULL,
					planresult TEXT DEFAULT '' NOT NULL,
					UNIQUE KEY id (id)
				)
				CHARACTER SET utf8
				COLLATE utf8_general_ci;";
			$wpdb->query($sql);

			$sql = "CREATE TABLE IF NOT EXISTS ".$wpdb->prefix.$this->form_table_customers." (
					id mediumint(9) NOT NULL AUTO_INCREMENT,
					email varchar(255) DEFAULT '' NOT NULL ,
					source varchar(255) DEFAULT '' NOT NULL ,
					customer_id varchar(255) DEFAULT '' NOT NULL ,
					customerresult TEXT DEFAULT '' NOT NULL,
					UNIQUE KEY id (id)
				)
				CHARACTER SET utf8
				COLLATE utf8_general_ci;";
			$wpdb->query($sql);

		} // end update_database


		/************************ PUBLIC METHODS  *****************************/


		/**
         * Inserts banks and Check if the Optional is enabled in the form, and inserts radiobutton
         */
        public function	insert_payment_fields( $form_code, $id )
		{
            global $wpdb;

			$rows = $wpdb->get_results(
						$wpdb->prepare( "SELECT * FROM ".$wpdb->prefix.$this->form_table." WHERE formid=%d", $id )
					);

			if (empty( $rows ) || !$rows[0]->enabled)
			    return $form_code;



            $checkscript = '';
            // $checkscript = '<script type="text/javascript">function cffaddonstripe'.$id.'(){ try { if(document.getElementById("cffaddonidtp'.$id.'").checked) document.getElementById("opfield'.$this->addonID.$id.'").style.display=""; else document.getElementById("opfield'.$this->addonID.$id.'").style.display="none"; } catch (e) {} }setInterval("cffaddonstripe'.$id.'()",200);</script>';
			$code = '<div id="opfield"'.$this->addonID.$id.'">'.
                      '<script src="https://checkout.stripe.com/checkout.js"></script>'.
                      '      <script>'.
                      '         var cpabc_stripe_handler_paid= false;'.
                      '         var cpabc_stripe_handler = StripeCheckout.configure({'.
                      '           key: \''.($rows[0]->stripe_mode=='0'?$rows[0]->stripe_testkey:$rows[0]->stripe_key).'\','.
                      '           image: \'\','.
                      '           token: function(token, args) {'.
                      '             document.getElementById("stptok'.$id.'").value = token.id;'.
                      '             cpabc_stripe_handler_paid = true;'.
                      '             doValidate_'.CPCFF_MAIN::$form_counter.'(document.getElementById("cp_calculatedfieldsf_pform_'.CPCFF_MAIN::$form_counter.'"));'.
                      '           }'.
                      '         });'.
                      '</script>'.
                      '<input type="hidden" name="stptok" id="stptok'.$id.'" value="" />'.
			        '</div>'.$checkscript;

			$form_code = preg_replace( '/<!--addons-payment-fields-->/i', '<!--addons-payment-fields-->'.$code, $form_code );

			// output radio-buttons here
			$form_code = preg_replace( '/<!--addons-payment-options-->/i', '<!--addons-payment-options--><div><input type="radio" name="bccf_payment_option_paypal" id="cffaddonidtp'.$id.'" vt="'.$this->addonID.'" value="'.$this->addonID.'" checked> '.__('Pay with Credit Cards', 'calculated-fields-form').'</div>', $form_code );

            if (($rows[0]->enabled == '2' || $rows[0]->enabled == '4') && !strpos($form_code,'bccf_payment_option_paypal" vt="0') )
			    $form_code = preg_replace( '/<!--addons-payment-options-->/i', '<!--addons-payment-options--><div><input type="radio" name="bccf_payment_option_paypal" vt="0" value="0"> '.__($this->_cpcff_main->get_form($id)->get_option('enable_paypal_option_no',CP_CALCULATEDFIELDSF_PAYPAL_OPTION_NO), 'calculated-fields-form').'</div>', $form_code );

			if (substr_count ($form_code, 'name="bccf_payment_option_paypal"') > 1)
			    $form_code = str_replace( 'id="field-c0" style="display:none">', 'id="field-c0">', $form_code);

            return $form_code;
		} // End insert_payment_fields


		/**
         * process payment
         */
		public function pp_stripe(&$params, &$str, $fields )
		{
            global $wpdb;

			// documentation: https://goo.gl/w3kKoH

            $rows = $wpdb->get_results(
						$wpdb->prepare( "SELECT * FROM ".$wpdb->prefix.$this->form_table." WHERE formid=%d", $params["formid"] )
					);

			$payment_option = (isset($_POST["bccf_payment_option_paypal"])?$_POST["bccf_payment_option_paypal"]:$this->addonID);
			if (empty( $rows ) || !$rows[0]->enabled || $payment_option != $this->addonID)
			    return;

            $params["payment_option"] = $this->name;
			$form_obj = $this->_cpcff_main->get_form($params['formid']);

            // **************
            require_once dirname( __FILE__ ) . '/stripe-php.addon/init.php';
            \Stripe\Stripe::setApiKey( ($rows[0]->stripe_mode=='0'?$rows[0]->stripe_testsecretkey:$rows[0]->stripe_secretkey) );

            // Get the credit card details submitted by the form
            $token = $_POST['stptok'];
            $amount = round($params["final_price"]*100,0);
            $currency = $form_obj->get_option('currency', CP_CALCULATEDFIELDSF_DEFAULT_CURRENCY);

            if (in_array( $rows[0]->frequency, array('day','week','month','year') ) )
            {
                // step 1: Identify / create plan
                // ***********************************
                $rowsplan = $wpdb->get_results(
						$wpdb->prepare( "SELECT * FROM ".$wpdb->prefix.$this->form_table_plans." WHERE planname=%s AND planinterval=%s AND currency=%s AND amount=%s AND trial_period_days=%s",
						   $rows[0]->planname, $rows[0]->frequency, $currency, $amount, $rows[0]->trialdays
						 )
					);
				if (!count($rowsplan))
				{
				    $wpdb->insert(
				                  $wpdb->prefix.$this->form_table_plans,
				                  array(
				                         'planname' => $rows[0]->planname,
				                         'planinterval' => $rows[0]->frequency,
				                         'currency' => $currency,
				                         'amount' => $amount,
				                         'trial_period_days' => "".$rows[0]->trialdays
						               ),
						          array ('%s', '%s', '%s', '%s', '%s')
					);
				    $planid = $wpdb->insert_id;
				    try {
                        $plan = \Stripe\Plan::create(array(
                          "name" => $rows[0]->planname,
                          "id" => "cffplan-".$planid,
                          "interval" => $rows[0]->frequency,
                          "currency" => $currency,
                          "trial_period_days" => $rows[0]->trialdays,
                          "amount" => $amount,
                        ));
                        $wpdb->update($wpdb->prefix.$this->form_table_plans,
				                        array('planresult' => serialize($plan)),
                  						array('id' => $planid)
					    );
                    } catch(Exception  $e) {
                        // The card has been declined
                        echo 'Stripe: Failed to create plan. Error message: '. $e->getMessage();
                        exit;
                    }
				}
				else
				    $planid = $rowsplan[0]->id;

                // step 2: Create customer
                // ***********************************
                try {
                    $notifyto = explode( ',', $form_obj->get_option('cu_user_email_field', '') );
                    $notifyto = $params[ $notifyto[0] ];
                    $customer = \Stripe\Customer::create(array(
                      "email" => $notifyto,
                      "source" => $token
                    ));
                    $wpdb->insert(
				                  $wpdb->prefix.$this->form_table_customers,
				                  array(
				                         'email' => $notifyto,
				                         'source' => $token,
				                         'customer_id' => $customer->id,
				                         'customerresult' => serialize($customer)
						               ),
						          array ('%s', '%s', '%s', '%s')
					);
                } catch(Exception  $e) {
                    // The card has been declined
                    echo 'Stripe: Failed to create customer. Error message: '. $e->getMessage();
                    exit;
                }

                // Step 3: Subscribe customer to plan
                // ***********************************
                try {
                    \Stripe\Subscription::create(array(
                      "customer" => $customer->id,
                      "plan" => "cffplan-".$planid,
                    ));
                } catch(Exception  $e) {
                    // The card has been declined
                    echo 'Stripe: Failed to create subscription. Error message: '. $e->getMessage();
                    exit;
                }


            }
            else{ // if no subscription then process one time payment_option
                // Create the charge on Stripe's servers - this will charge the user's card
                try {
                    $charge = \Stripe\Charge::create(array(
                      "amount" => $amount, // amount in cents, again
                      "currency" => $currency,
                      "card" => $token,
                      "description" => $form_obj->get_option('paypal_product_name', CP_CALCULATEDFIELDSF_DEFAULT_PRODUCT_NAME))
                    );
                } catch(Stripe_CardError $e) {
                    // The card has been declined
                    echo 'Transaction failed. The card has been declined. Please <a href="javascript:window.history.back();">go back and try again</a>.';
                    exit;
                }
            }
            // **************

		} // end pp_stripe

        function pp_stripe_redirect($params)
        {
            global $wpdb;
            $row = $wpdb->get_row(
				$wpdb->prepare( "SELECT * FROM ".$wpdb->prefix.$this->form_table." WHERE formid=%d", $params["formid"] )
			);

			$payment_option = (isset($_POST["bccf_payment_option_paypal"])?$_POST["bccf_payment_option_paypal"]:$this->addonID);
			if (
				empty( $row )  ||
				!$row->enabled ||
				$payment_option != $this->addonID
			) return;

			// mark item as paid
			CPCFF_SUBMISSIONS::update($params["itemnumber"], array('paid'=>1));
			$form_obj = CPCFF_SUBMISSIONS::get_form($params["itemnumber"]);
			// if($form_obj->get_option('paypal_notiemails', '0') != '1')   this line has been commented out, this question doesn't apply here
			$this->_cpcff_main->send_mails($params['itemnumber']);
            do_action( 'cpcff_payment_processed', $params );
            $redirect = true;

		    /**
		     * Filters applied to decide if the website should be redirected to the thank you page after submit the form,
		     * pass a boolean as parameter and returns a boolean
		     */
            $redirect = apply_filters( 'cpcff_redirect', $redirect );

            if( $redirect )
            {
                $location = $form_obj->get_option('fp_return_page', CP_CALCULATEDFIELDSF_DEFAULT_fp_return_page);
                header("Location: ".$location);
                exit;
            }
        }

		/**
		 * mark the item as paid
		 */
		private function _log($adarray = array())
		{
			$h = fopen( __DIR__.'/logs.txt', 'a' );
			$log = "";
			foreach( $_REQUEST as $KEY => $VAL )
			{
				$log .= $KEY.": ".$VAL."\n";
			}
			foreach( $adarray as $KEY => $VAL )
			{
				$log .= $KEY.": ".$VAL."\n";
			}
			$log .= "================================================\n";
			fwrite( $h, $log );
			fclose( $h );
		}


        /**
         * script process payment
         */
		public function pp_stripe_check_price()
		{
            global $wpdb;

            if (!isset($_POST["cpcff_stripe_getprice"]) || !isset($_POST["formid"]))
                return;

            $form_id = $_POST["formid"];

            $rows = $wpdb->get_results(
						$wpdb->prepare( "SELECT * FROM ".$wpdb->prefix.$this->form_table." WHERE formid=%d", $form_id )
					);

			if (empty( $rows ) || !$rows[0]->enabled)
			    return;

			$form_obj = $this->_cpcff_main->get_form($form_id);
	        $find_arr = array( ',', '.');
	        $replace_arr = array( '', '.');
			$price = preg_replace( '/[^\d\.\,]/', '', $_POST["ps"] );
			$price = str_replace( $find_arr, $replace_arr, $price );
	        $paypal_base_amount = preg_replace('/[^\d\.\,]/', '', $form_obj->get_option('paypal_base_amount', 0));
	        $paypal_base_amount = str_replace( $find_arr, $replace_arr, $paypal_base_amount );
	        $price = max( $price, $paypal_base_amount );

            // calculate discounts if any
            //---------------------------
            $discount_note = "";
            $coupon = false;
            $codes = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM ".CP_CALCULATEDFIELDSF_DISCOUNT_CODES_TABLE_NAME." WHERE code=%s AND expires>='".date("Y-m-d")." 00:00:00' AND `form_id`=%d", @$_POST["couponcode"], $form_id  ) );
            if (count($codes))
            {
                $coupon = $codes[0];
               if ($coupon->availability==1)
                {
	        		$coupon->discount = str_replace( $find_arr, $replace_arr, $coupon->discount );
                    $price = number_format (floatval ($price) - $coupon->discount,2);
                    $discount_note = " (".$form_obj->get_option('currency', CP_CALCULATEDFIELDSF_DEFAULT_CURRENCY)." ".$coupon->discount." discount applied)";
                }
                else
                {
                    $price = number_format (floatval ($price) - $price*$coupon->discount/100,2);
                    $discount_note = " (".$coupon->discount."% discount applied)";
                }
            }

			echo round($price*100,0);
			exit;

         }



		/**
         * script process payment
         */
		public function pp_payments_script( $form_sequence_id, $form_id )
		{
            global $wpdb;

            $rows = $wpdb->get_results(
						$wpdb->prepare( "SELECT * FROM ".$wpdb->prefix.$this->form_table." WHERE formid=%d", $form_id )
					);

			if (empty( $rows ) || !$rows[0]->enabled)
			    return;

			$form_obj = $this->_cpcff_main->get_form($form_id);
            $currency = $form_obj->get_option('currency', CP_CALCULATEDFIELDSF_DEFAULT_CURRENCY);
?>
            if (!cpabc_stripe_handler_paid && $dexQuery("input[name='bccf_payment_option_paypal']:checked").val() == '<?php echo $this->addonID; ?>')
            {
                $dexQuery.ajax({
                    type: "POST",
                    url: '<?php echo CPCFF_AUXILIARY::site_url(); ?>/',
                    data: {
						ps: document.getElementById("<?php echo $form_obj->get_option('request_cost', CP_CALCULATEDFIELDSF_DEFAULT_COST).$form_sequence_id; ?>").value,
						formid: <?php echo $form_id; ?>,
						cpcff_stripe_getprice: "1"
					},
                    success: function(data)
                    {
                        if (parseFloat(data) > 0)
                        {
                            cpabc_stripe_handler.open({
                              name: '<?php echo str_replace("'","\'",$form_obj->get_option('paypal_product_name',CP_CALCULATEDFIELDSF_DEFAULT_PRODUCT_NAME)); ?>',
                              description: '<?php echo str_replace("'","\'",$rows[0]->stripe_subtitle); ?>',
                              image: '<?php echo str_replace("'","\'",$rows[0]->stripe_logoimage); ?>',
                              currency: '<?php echo $currency; ?>',
                              amount: parseFloat(data)
                            });
                        }
                        else
                            alert('CFF Configuration error: cannot get price from server');
                    }
                });

                return false;
            }
<?php
        }


        function add_field_verify ($field, $type = "varchar(255) DEFAULT '' NOT NULL", $table = '')
        {
            global $wpdb;
            if ($table == '') $table = $wpdb->prefix.$this->form_table;
			if(empty($this->table_columns))
			{
				$columns = $wpdb->get_results("SHOW columns FROM `".$table."`");
				foreach( $columns as $column ) $this->table_columns[$column->Field] = $column->Field;
			}
            if (empty($this->table_columns[$field]))
            {
                $sql = "ALTER TABLE  `".$table."` ADD `".$field."` ".$type;
                $wpdb->query($sql);
            }
        }

		/**
		 *	Delete the form from the addon's table
		 */
        public function delete_form( $formid)
		{
			global $wpdb;
			$wpdb->delete( $wpdb->prefix.$this->form_table, array('formid' => $formid), '%d' );
		} // delete_form

        		/**
		 *	Clone the form's row
		 */
		public function clone_form( $original_form_id, $new_form_id )
		{
			global $wpdb;

			$form_rows = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM ".$wpdb->prefix.$this->form_table." WHERE formid=%d", $original_form_id ), ARRAY_A);

			if(!empty($form_rows))
			{
				foreach($form_rows as $form_row)
				{
					unset($form_row["id"]);
					$form_row["formid"] = $new_form_id;
					$wpdb->insert( $wpdb->prefix.$this->form_table, $form_row);
				}
			}
		} // End clone_form

		/**
		 *	It is called when the form is exported to export the addons data too.
		 *  Receive an array with the other addons data, and the form's id for filtering.
		 */
		public function export_form($addons_array, $formid)
		{
			global $wpdb;
			$rows = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM ".$wpdb->prefix.$this->form_table." WHERE formid=%d", $formid ), ARRAY_A );
			if(!empty($rows))
			{
				$addons_array[ $this->addonID ] = array();
				foreach($rows as $row)
				{
					unset($row['id']);
					unset($row['formid']);
					$addons_array[ $this->addonID ][] = $row;
				}
			}
			return $addons_array;
		} // End export_form

		/**
		 *	It is called when the form is imported to import the addons data too.
		 *  Receive an array with all the addons data, and the new form's id.
		 */
		public function import_form($addons_array, $formid)
		{
			global $wpdb;
			if(isset($addons_array[$this->addonID]))
			{
				foreach($addons_array[$this->addonID] as $row)
				{
					if(!empty($row))
					{
						$row['formid'] = $formid;
						$wpdb->insert(
							$wpdb->prefix.$this->form_table,
							$row
						);
					}
				}
			}
		} // End import_form


    } // End Class

    // Main add-on code
    $CPCFF_Stripe_obj = new CPCFF_Stripe();

    CPCFF_ADDONS::add($CPCFF_Stripe_obj);
}
?>