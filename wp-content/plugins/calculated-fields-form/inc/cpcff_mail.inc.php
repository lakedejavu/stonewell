<?php
/**
 * CPCFF_MAIL process and sends the notification and confirmation emails
 *
 * @package CFF.
 * @since 5.0.216 (PRO), 5.0.257 (DEV), 10.0.288 (PLA)
 */

if(!class_exists('CPCFF_MAIL'))
{
	/**
	 * Class that sends the notification emails processing first the emails and subjects
	 *
	 * @since 5.0.215 (PRO), 5.0.256 (DEV), 10.0.287 (PLA)
	 */
	class CPCFF_MAIL
	{
		/**
		 * Submission ID
		 *
		 * @var int $_id
		 */
		private $_submission_obj;
		private $_form_obj;
		private $_from;
		private $_phpmailer_from;

		/**
		 * Send the notification emails to the email address entered through the form's settings
		 *
		 *
		 * @return boolean.
		 */
		public function send_notification_email($submission_id)
		{
			$this->_init($submission_id);
			$submission_obj = $this->_submission_obj;
			if($submission_obj)
			{
				$form_obj = $this->_form_obj;

				$fields = $form_obj->get_fields();
				$fields[ 'ipaddr' ] = $submission_obj->ipaddr;

				add_filter('phpmailer_init', array($this, 'phpmailer_init'));

				$email_data = CPCFF_AUXILIARY::parsing_fields_on_text(
					$fields,
					$submission_obj->paypal_post,
					$form_obj->get_option('fp_message', CP_CALCULATEDFIELDSF_DEFAULT_fp_message),
					$submission_obj->data,
					$form_obj->get_option('fp_emailformat', CP_CALCULATEDFIELDSF_DEFAULT_email_format),
					$submission_id
				);


				if ('true' == $form_obj->get_option('fp_inc_additional_info', CP_CALCULATEDFIELDSF_DEFAULT_fp_inc_additional_info))
				{
					$basic_data = "IP: ".$submission_obj->ipaddr."\nServer Time:  ".date("Y-m-d H:i:s")."\n";
					/**
					 *	Includes additional information to the email's message,
					 *  are passed two parameters: the basic information, and the IP address
					 */
					$basic_data = apply_filters( 'cpcff_additional_information',  $basic_data, $submission_obj->ipaddr );
					$email_data[ 'text' ] .= "ADDITIONAL INFORMATION\n*********************************\n".$basic_data;
				}

				$subject = $form_obj->get_option('fp_subject', CP_CALCULATEDFIELDSF_DEFAULT_fp_subject);
				$subject = CPCFF_AUXILIARY::parsing_fields_on_text(
					$fields,
					$submission_obj->paypal_post,
					$subject,
					'',
					'plain text',
					$submission_id
				);

				$to = CPCFF_AUXILIARY::parsing_fields_on_text(
					$fields,
					$submission_obj->paypal_post,
					preg_replace("/(fieldname\d+)\s*%>/i", "$1_value%>", $form_obj->get_option('fp_destination_emails', CP_CALCULATEDFIELDSF_DEFAULT_fp_destination_emails)),
					'',
					'plain text',
					$submission_id
				);

				$to = explode(
					",",
					$to['text']
				);

				if ('html' == $form_obj->get_option('fp_emailformat', CP_CALCULATEDFIELDSF_DEFAULT_email_format))
				{
					$content_type = "Content-Type: text/html; charset=utf-8\n";
				}
				else $content_type = "Content-Type: text/plain; charset=utf-8\n";

				$replyto = explode( ',', $submission_obj->notifyto );

				if ($form_obj->get_option('fp_emailfrommethod', "fixed") == "customer" && !empty( $replyto ) )
					$from = $replyto[ 0 ];
				else
					$from = $this->_get_from();

				$this->_phpmailer_from = $from;

				if ( strpos($from,">") === false ) $from = '"'.$from.'" <'.$from.'>';

				foreach ($to as $item)
				{
					if (trim($item) != '')
					{
						try
						{
							wp_mail(
								trim($item),
								$subject[ 'text' ],
								$email_data[ 'text' ],
								"From: ".$from."\r\n".
								( ( !empty( $replyto ) ) ? "Reply-To: ".str_replace(' ', '', implode( ',', $replyto ))."\r\n" : "" ).
								$content_type.
								"X-Mailer: PHP/" . phpversion(), $email_data[ 'files' ]
							);
						}
						catch( Exception $mail_err ){}
					}
				}

				remove_filter('phpmailer_init', array($this,'phpmailer_init'));
			}
		} // End send_notification_email

		/**
		 * Sends the copy email to the users (using the email address submitted through the form),
		 * or to the email address passed as parameter
		 *
		 * @param string $email email address, default an empty string.
		 * @return boolean.
		 */
		public function send_confirmation_email( $submission_id, $email = '' )
		{
			$this->_init($submission_id);
			$submission_obj = $this->_submission_obj;
			if($submission_obj)
			{
				$form_obj = $this->_form_obj;
				$notifyto = explode( ',', $submission_obj->notifyto ); // Allows send multiple notification emails.

				if(
					(!empty($notifyto) || $email != '') &&
					'true' == $form_obj->get_option('cu_enable_copy_to_user', CP_CALCULATEDFIELDSF_DEFAULT_cu_enable_copy_to_user)
				)
				{
					$fields = $form_obj->get_fields();
					$fields[ 'ipaddr' ] = $submission_obj->ipaddr;

					add_filter('phpmailer_init', array($this, 'phpmailer_init'));

					$email_data = CPCFF_AUXILIARY::parsing_fields_on_text(
                        $fields,
                        $submission_obj->paypal_post,
                        $form_obj->get_option('cu_message', CP_CALCULATEDFIELDSF_DEFAULT_cu_message),
                        $submission_obj->data,
                        $form_obj->get_option('cu_emailformat', CP_CALCULATEDFIELDSF_DEFAULT_email_format),
                        $submission_id
                    );

					$subject = $form_obj->get_option('cu_subject', CP_CALCULATEDFIELDSF_DEFAULT_cu_subject);
					$subject = CPCFF_AUXILIARY::parsing_fields_on_text(
						$fields,
						$submission_obj->paypal_post,
						$subject,
						'',
						'plain text',
						$submission_id
					);

					if ('html' == $form_obj->get_option('cu_emailformat', CP_CALCULATEDFIELDSF_DEFAULT_email_format))
					{
						$content_type = "Content-Type: text/html; charset=utf-8\n";
					}
					else $content_type = "Content-Type: text/plain; charset=utf-8\n";

					$from = $this->_get_from();
					$this->_phpmailer_from = $from;

					if ( strpos($from,">") === false ) $from = '"'.$from.'" <'.$from.'>';
					if ( !in_array( $email, $notifyto ) && $email != '') $notifyto[] = $email;
					if ( !empty( $notifyto ) )
					{
						foreach( $notifyto as $email_address )
						{
							try
							{
								wp_mail(
									trim($email_address),
									$subject[ 'text' ],
									$email_data[ 'text' ],
									"From: ".$from."\r\n".
									$content_type.
									"X-Mailer: PHP/" . phpversion()
								);
							}
							catch( Exception $mail_err ){}
						}
					}
				}
				remove_filter('phpmailer_init',array($this, 'phpmailer_init'));
			}
		} // End send_confirmation_email

		public function phpmailer_init( $phpmailer )
		{
			// Checks if the email's headers should be corrected or not
			if( !get_option( 'CP_CALCULATEDFIELDSF_EMAIL_HEADERS', false ) ) return $phpmailer;

			$from = (!empty($this->_phpmailer_from)) ? $this->_phpmailer_from : $this->_get_from();
			$from = strtolower($from);

			$parts 		= explode('@', $from);
			$home_url 	= CPCFF_AUXILIARY::site_url();

			if(
				strtolower( $phpmailer->Mailer ) == 'smtp' ||
				count( $parts ) != 2 ||
				strpos( $home_url, $parts[ 1 ] ) === false
			) return $phpmailer;

			$phpmailer->Sender 	= $from;
			$phpmailer->From 	= $from;

			return $phpmailer;
		} // End phpmailer_init

		/*********************************** PRIVATE METHODS  ********************************************/

		private function _init( $submission_id )
		{
			$this->_submission_obj = CPCFF_SUBMISSIONS::get($submission_id);
			if($this->_submission_obj) $this->_form_obj = CPCFF_SUBMISSIONS::get_form($submission_id);
		} // End _init

		private function _get_from()
		{
			if(empty($this->_from))
			{
				$from = CPCFF_AUXILIARY::parsing_fields_on_text(
					$this->_form_obj->get_fields(),
					$this->_submission_obj->paypal_post,
					preg_replace(
						"/(fieldname\d+)\s*%>/i",
						"$1_value%>",
						$this->_form_obj->get_option('fp_from_email', CP_CALCULATEDFIELDSF_DEFAULT_fp_from_email)
					),
					'',
					'plain text',
					$this->_submission_obj->id
				);
				$this->_from = $from['text'];
			}

			return $this->_from;
		} // End _get_from

	} // End CPCFF_MAIL
}