<?php
/**
 * Widgets Classes and related code
 *
 * @package CFF.
 * @since 1.0.177
 */

// WIDGET CODE BELOW
// ***********************************************************************
if(!class_exists( 'CPCFF_WIDGET' ) )
{
	/**
	 * Widget class to insert the forms on sidebars extends the WP_Widget class.
	 *
	 * @since  1.0.178
	 */
	class CPCFF_WIDGET extends WP_Widget
	{
		/**
		 * Object of the main plugin's class
		 * Instance property.
		 *
		 * @var object $_cpcff_main
		 */
		private $_cpcff_main;

		/**
		 * Class construct
		 *
		 * @param object $_cpcff_main instance of the CPCFF_MAIN class
		 */
		function __construct($_cpcff_main)
		{
			$this->_cpcff_main = $_cpcff_main;
			$widget_ops = array('classname' => 'CP_calculatedfieldsf_Widget', 'description' => 'Displays a form integrated with Paypal' );
			parent::__construct('CP_calculatedfieldsf_Widget', 'Calculated Fields Form', $widget_ops);
		}

		function form($instance)
		{
			$instance = wp_parse_args( (array) $instance, array( 'title' => '', 'formid' => '' ) );
			$title = $instance['title'];
			$formid = $instance['formid'];
			?>
			<p>
				<label for="<?php echo $this->get_field_id('title'); ?>">Title: <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" /></label>
				<label for="<?php echo $this->get_field_id('formid'); ?>">Form ID: <input class="widefat" id="<?php echo $this->get_field_id('formid'); ?>" name="<?php echo $this->get_field_name('formid'); ?>" type="text" value="<?php echo esc_attr($formid); ?>" /></label>
			</p>
			<?php
		}

		function update($new_instance, $old_instance)
		{
			$instance = $old_instance;
			$instance['title'] = $new_instance['title'];
			$instance['formid'] = $new_instance['formid'];
			return $instance;
		}

		function widget($args, $instance)
		{
			extract($args, EXTR_SKIP);

			echo $before_widget;
			$title = empty($instance['title']) ? ' ' : apply_filters('widget_title', $instance['title']);
			$formid = $instance['formid'];

			if (!empty($title))
			  echo $before_title . $title . $after_title;

			echo $this->_cpcff_main->public_form( array( 'id' => $formid ) );
			echo $after_widget;
		}
	} // End CPCFF_WIDGET
}

if(!class_exists('CPCFF_DASHBOART_WIDGET'))
{
	/**
	 * Class to publish a dashboard widget CPCFF_DASHBOART_WIDGET
	 *
	 * @since  1.0.178
	 */
	class CPCFF_DASHBOART_WIDGET
	{
		private $_cpcff_main; // Main object it is not used for now.

		/**
		 * Class construct
		 *
		 * @param object $_cpcff_main instance of the CPCFF_MAIN class
		 */
		public function __construct( $_cpcff_main )
		{
			$this->_cpcff_main = $_cpcff_main;

			if( !current_user_can( 'manage_options' ) ) return;
			wp_add_dashboard_widget(
				'cp_calculatedfieldsf_dashboard_widgets',
				'Calculated Fields Form Activity',
				array($this, 'dashboard_widget')
			);
		} // End __construct

		/**
		 * Generates html code to display in the dashboard with the information collected by the form.
		 *
		 * Prints the HMTL code directly to the browser.
		 */
		public function dashboard_widget()
		{
			global $wpdb;

			$styleA = 'style="border-right:1px solid rgb(238, 238, 238);border-bottom:1px solid rgb(238, 238, 238);"';
			$styleB = 'style="border-bottom:1px solid rgb(238, 238, 238);"';
			$styleC = 'style="color:#FF0000;"';
			$styleD = 'style="font-weight:bold;"';
			?>
			<div style="max-height:400px; overflow-y:auto;">
			<table style="width:100%;">
				<tr>
					<th align="left" <?php echo $styleA; ?>><?php _e( 'ID', 'calculated-fields-form' ); ?></th>
					<th align="left" <?php echo $styleA; ?>><?php _e( 'Form', 'calculated-fields-form' ); ?></th>
					<th align="left" <?php echo $styleA; ?>><?php _e( 'Date', 'calculated-fields-form' ); ?></th>
					<th align="left" <?php echo $styleA; ?>><?php _e( 'Email', 'calculated-fields-form' ); ?></th>
					<th align="left" <?php echo $styleB; ?>><?php _e( 'Payment Info', 'calculated-fields-form' ); ?></th>
				</tr>
			<?php
			/* TO-DO: This method should be analyzed after moving other functions to the main class . */
			$submissions_result = $wpdb->get_results( "SELECT ftable.form_name, ptable.* FROM ".$wpdb->prefix.CP_CALCULATEDFIELDSF_FORMS_TABLE." as ftable, ".CP_CALCULATEDFIELDSF_POSTS_TABLE_NAME." as ptable WHERE ptable.formid=ftable.id AND ptable.time > CURDATE() - INTERVAL 7 DAY ORDER BY ptable.time DESC;" );

			foreach( $submissions_result as $row )
			{
				// Add links
				$paypal_post = @unserialize( $row->paypal_post );
				$_urls = '';

				if( $paypal_post !== false )
				{
					foreach( $paypal_post as $_key => $_value )
					{
						if( strpos( $_key, '_url' ) )
						{
							if( is_array( $_value ) )
							{
								foreach( $_value as $_url )
								{
									$_urls .= '<p><a href="'.esc_attr( $_url ).'" target="_blank">'.$_url.'</a></p>';
								}
							}
						}
					}
				}

				echo '
				<tr>
					<td align="left" '.$styleA.'><span '.$styleD.'>'.$row->id.'</span></td>
					<td align="left" '.$styleA.'>
						<a href="admin.php?page=cp_calculated_fields_form&cal='.$row->formid.'&list=1&r='.rand().'">'.$row->form_name.'</a>
					</td>
					<td align="left" '.$styleA.'>'.$row->time.'</td>
					<td align="left" '.$styleA.'>'.$row->notifyto.'</td>
					<td align="left" '.$styleB.'>'.( ( $row->paid ) ? __('Paid', 'calculated-fields-form' ) : '<span '.$styleC.'>'.__('Not Paid', 'calculated-fields-form' ).'</span>' ).'</td>
				</tr>
				<tr>
					<td colspan="5"  '.$styleB.' >
					'.preg_replace(
							'/\n+/',
							'<br>',
							str_replace(
							array( "\'", '\"' ),
							array( "'", '"' ),
							$row->data)
					 ).$_urls.'
					</td>
				</tr>
				';
			}
			?>
			</table>
			</div>
			<?php
		} // End dashboard_widget
	} // End CPCFF_DASHBOART_WIDGET
}