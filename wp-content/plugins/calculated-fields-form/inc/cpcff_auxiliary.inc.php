<?php
/**
 * Miscellaneous operations: CPCFF_AUXILIARY class
 *
 * Metaclass with miscellanous operations used through all plugin.
 *
 * @package CFF.
 * @since 1.0.167
 */

if(!class_exists('CPCFF_AUXILIARY'))
{
	/**
	 * Metaclass with miscellaneous operations.
	 *
	 * Publishes miscellanous operations to be used through all plugin's sections.
	 *
	 * @since  1.0.167
	 */
	class CPCFF_AUXILIARY
	{
		/**
		 * Public URL of the current blog.
		 *
		 * @since 1.0.167
		 * @var string $_site_url
		 */
		private static $_site_url;

		/**
		 * URL to the WordPress of the current blog.
		 *
		 * @since 1.0.167
		 * @var string $_wp_url
		 */
		private static $_wp_url;

		/**
		 * Returns the public URL of the current blog.
		 *
		 * If the URL was read previously, uses the value stored in class property.
		 *
		 * @since 1.0.167
		 * @return string.
		 */
		public static function site_url()
		{
			if(empty(self::$_site_url))
			{
				$blog = get_current_blog_id();
				self::$_site_url = get_home_url( $blog, '', is_ssl() ? 'https' : 'http');
			}
			return rtrim(self::$_site_url, '/');
		} // End site_url

		/**
		 * Returns the URL to the WordPress of the current blog.
		 *
		 * If the URL was read previously, uses the value stored in class property.
		 *
		 * @since 1.0.167
		 * @return string.
		 */
		public static function wp_url()
		{
			if(empty(self::$_wp_url))
			{
				$blog = get_current_blog_id();
				self::$_wp_url = get_admin_url( $blog );
			}
			return rtrim(self::$_wp_url, '/');
		} // End wp_url

		/**
		 * Sanitizes the value received as parameter, supporting the same posts tags
		 *
		 * @since Pro 5.0.235, Dev 5.0.279, Plat 10.0.318
		 *
		 * @params mixed $v.
		 * @return sanitized value.
		 */
		public static function sanitize( $v )
		{
			$allowed_tags = wp_kses_allowed_html( 'post' );
			return wp_kses($v, $allowed_tags);
		} // End sanitize

		/**
		 * Removes Bom characters.
		 *
		 * @since 1.0.179
		 *
		 * @param string $str.
		 * @return string.
		 */
		public static function clean_bom($str)
		{
			$bom = pack('H*','EFBBBF');
			return preg_replace("/$bom/", '', $str);
		} // End clean_bom

		/**
		 * Converts some characters in a JSON string.
		 *
		 * @since 1.0.169
		 *
		 * @param string $str JSON string.
		 * @return string.
		 */
		public static function clean_json($str)
		{
			return str_replace(
				array("	", "\n", "\r"),
				array(" ", '\n', ''),
				$str
			);
		} // End clean_json

		/**
		 * Decodes a JSON string.
		 *
		 * Decode a JSON string, and receive a parameter to apply strip slashes first or not.
		 *
		 * @since 1.0.169
		 *
		 * @param string $str JSON string.
		 * @param string $stripcslashes Optional. To apply a stripcslashes to the text before json_decode. Default 'unescape'.
		 * @return mixed PHP Oject or False.
		 */
		public static function json_decode($str, $stripcslashes = 'unescape')
		{
			try
			{
				$str = CPCFF_AUXILIARY::clean_json( $str );
				if( $stripcslashes == 'unescape')$str = stripcslashes( $str );
				$obj = json_decode( $str );
			}
			catch( Exception $err ){ self::write_log($err); }
			return ( !empty( $obj ) ) ? $obj : false;
		} // End unserialize

		/**
		 * Replaces recursively the elements in an array by the elements in another one.
		 *
		 * The method will use the PHP function: array_replace_recursive if exists.
		 *
		 * @since 1.0.169
		 *
		 * @param array $array1
		 * @param array $array2
		 * @return array
		 */
		public static function array_replace_recursive($array1, $array2)
		{
			// If the array_replace_recursive function exists, use it
			if(function_exists('array_replace_recursive')) return array_replace_recursive($array1, $array2);
			foreach( $array2 as $key1 => $val1 )
			{
				if( isset( $array1[ $key1 ] ) )
				{
					if( is_array( $val1 ) )
					{
						foreach( $val1 as $key2 => $val2)
						{
							$array1[ $key1 ][ $key2 ] = $val2;
						}
					}
					else
					{
						$array1[ $key1 ] = $val1;
					}
				}
				else
				{
					$array1[ $key1 ] = $val1;
				}
			}
			return $array1;
		} // End array_replace_recursive

		/**
		 * Applies stripcslashes to the array elements recursively.
		 *
		 * The method checks if parameter is an array a text. If it is an array the method is called recursively.
		 *
		 * @since 1.0.176
		 *
		 * @param mixed $v array or single value.
		 * @return mixed the array or value with the slashes stripped
		 */
		public static function stripcslashes_recursive( $v )
		{
			if(is_array($v))
			{
				foreach($v as $k => $s)
				{
					$v[$k] = self::stripcslashes_recursive($s);
				}
				return $v;
			}
			else
			{
				return stripcslashes($v);
			}
		} // End stripcslashes_recursive

		/**
		 * Checks if the website is being visited by a crawler.
		 *
		 * Returns true if the website is being visited by a search engine spider,
		 * and the plugin was configure for hidding the forms front them, else false.
		 *
		 * @since 1.0.169
		 *
		 * @return bool.
		 */
		public static function is_crawler()
		{
			return (isset( $_SERVER['HTTP_USER_AGENT'] ) &&
					preg_match( '/bot|crawl|slurp|spider/i', $_SERVER[ 'HTTP_USER_AGENT' ] ) &&
					get_option( 'CP_CALCULATEDFIELDSF_EXCLUDE_CRAWLERS', false )
				);
		} // End is_crawler

		/**
		 * Checks if the page is AMP or not
		 *
		 * Checks first for the existence of functions: "is_amp_endpoint" or "ampforwp_is_amp_endpoint",
		 * and if they don't exists, checks the URL.
		 *
		 * @since 1.0.190
		 *
		 * @return bool.
		 */
		public static function is_amp()
		{
			if( function_exists('ampforwp_is_amp_endpoint') ) return ampforwp_is_amp_endpoint();
			elseif( function_exists('is_amp_endpoint') )
			{
				if(defined('AMP_QUERY_VAR')) return is_amp_endpoint();
			}
			return false;
		} // End is_amp

		/**
		 * Returns an iframe tag for loading the a webpage with the form only, specially useful for AMP pages.
		 *
		 * @since 1.0.190
		 * @return string, the iframe tag's structure for loading a page with the form.
		 */
		public static function get_iframe( $atts )
		{
			$url = self::site_url();
			$url = preg_replace('/^http\:/i', 'https:', $url);
			$url .= (strpos($url, '?') === false) ? '?'	: ':';
			$url .= 'cff-form='.((!empty($atts['id']))?$atts['id'] : '');
			$height = '';
			foreach($atts as $attr_name => $attr_value)
			{
				if('amp_iframe_height' == $attr_name) $height = $attr_value;
				elseif('id' != $attr_name) $url .= '&cff-form-attr-'.$attr_name.'='.$attr_value;
			}

			if(empty($height))  $height = 500;

			$url .= '&cff-form-height='.$height;

			// Fixing the isseu with the origin policy in the amp-iframes
			if(preg_match('/^https:\/\/www\./i', $url)) $url = preg_replace('/^https:\/\/www\./i', 'https://', $url);
			else  $url = preg_replace('/^https:\/\//i', 'https://www.', $url);

			add_action('amp_post_template_css', array('CPCFF_AUXILIARY', 'amp_css') );
			add_filter( 'amp_post_template_data', array('CPCFF_AUXILIARY', 'amp_iframe') );

			return '<amp-iframe id="cff-form-iframe" src="'.esc_attr( esc_url($url)).'" layout="fixed-height" sandbox="allow-popups allow-forms allow-top-navigation allow-modals allow-scripts allow-same-origin" height="'.esc_attr($height).'"><amp-img src="data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+PHN2ZyB4bWxuczpkYz0iaHR0cDovL3B1cmwub3JnL2RjL2VsZW1lbnRzLzEuMS8iIHhtbG5zOmNjPSJodHRwOi8vY3JlYXRpdmVjb21tb25zLm9yZy9ucyMiIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyIgeG1sbnM6c3ZnPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiBpZD0ic3ZnOCIgdmVyc2lvbj0iMS4xIiB2aWV3Qm94PSIwIDAgMTMuMjI5MTY3IDEzLjIyOTE2NyIgaGVpZ2h0PSI1MCIgd2lkdGg9IjUwIj48ZGVmcyBpZD0iZGVmczIiIC8+PG1ldGFkYXRhIGlkPSJtZXRhZGF0YTUiPjxyZGY6UkRGPjxjYzpXb3JrIHJkZjphYm91dD0iIj48ZGM6Zm9ybWF0PmltYWdlL3N2Zyt4bWw8L2RjOmZvcm1hdD48ZGM6dHlwZSByZGY6cmVzb3VyY2U9Imh0dHA6Ly9wdXJsLm9yZy9kYy9kY21pdHlwZS9TdGlsbEltYWdlIiAvPjxkYzp0aXRsZT48L2RjOnRpdGxlPjwvY2M6V29yaz48L3JkZjpSREY+PC9tZXRhZGF0YT48ZyB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLC0yODMuNzcwODMpIiBpZD0ibGF5ZXIxIiAvPjwvc3ZnPg==" placeholder layout="responsive" width="50" height="50" /></amp-iframe>';
		}

		/**
		 * Includes the CSS rules for the amp version of form
		 *
		 * @sinze 1.0.190
		 *
		 * @param object, template.
		 */
		public static function amp_css($template)
		{
			print '#cff-form-iframe{margin:0;}';
		} // End amp_css

		/**
		 * Checks if the amp-iframe.js was included, and includes it if not.
		 *
		 * @since 1.0.193
		 * @param $data, associative array.
		 * @return $data, associative array.
		 */
		public static function amp_iframe($data)
		{
			if ( empty( $data['amp_component_scripts']['amp-iframe'] ) )
			{
				$data['amp_component_scripts']['amp-iframe'] = 'https://cdn.ampproject.org/v0/amp-iframe-0.1.js';
			}
			return $data;
		} // End amp_iframe

		/**
		 * Converts the corresponding parameters in an associative array.
		 *
		 * The parameters with the name cff-form is converted in the id attribute,
		 * and the parameteres with the name:  cff-form-attr-<param>, are converted in the attributes <param>
		 *
		 * @since 1.0.190
		 * @return array $attrs.
		 */
		public static function params_to_attrs()
		{
			$attrs = array();
			if(!empty($_GET))
			{
				foreach($_GET as $param => $value)
				{
					if( $param == 'cff-form')
						$attrs['id'] = @intval($value);
					elseif(preg_match('/^cff\-form\-attr\-/i', $param))
						$attrs[preg_replace('/^cff\-form\-attr\-/i', '', $param)] = $value;
				}
			}
			return $attrs;
		} // End params_to_attrs

		/**
		 * Checks if the uploaded file is supported by WordPress and it is not a dangerous  file.
		 *
		 * Executables and scripts are considered as potencially dangerous files.
		 *
		 * @since 1.0.178
		 *
		 * @param array $file with the file's name and the temporal name after upload it.
		 * @return bool.
		 */
		public static function check_uploaded_file($file)
		{
			$filetmp = $file['tmp_name'];
			$filename = $file['name'];

			// Get file info
			$filetype = wp_check_filetype( basename( $filename ), null );

			// Excluding dangerous files
			if (
				$filetype["ext"] == false ||
				in_array ($filetype["ext"],array("php","asp","aspx","cgi","pl","perl","exe"))
			)
				return false;

			return true;
		} // End check_uploaded_file

		/**
		 * Adds the attribute: property="stylesheet" to the link tag to validate the link tags into the pages' bodies.
		 *
		 * Checks if it is an stylesheet and adds the property if has not been included previously.
		 *
		 * @since 1.0.178
		 *
		 * @param string $tag the link tag.
		 * @return string.
		 */
		public static function complete_link_tag( $tag )
		{
			if(
				preg_match('/stylesheet/i', $tag) &&
				!preg_match('/property\s*=/i', $tag)
			)
			{
				return str_replace( '/>', ' property="stylesheet" />', $tag );
			}
			return $tag;
		} // End complete_link_tag

		/**
		 * Creates a new entry in the PHP Error Logs.
		 *
		 * @since 1.0.167
		 *
		 * @param mixed $log Log message, as text, array or plain object.
		 * @return void.
		 */
		public static function write_log($log)
		{
			try{
				if(
					defined('WP_DEBUG') &&
					true == WP_DEBUG
				)
				{
					if(
						is_array( $log ) ||
						is_object( $log )
					)
					{
						error_log( print_r( $log, true ) );
					}
					else
					{
						error_log( $log );
					}
				}
			}catch(Exception $err){}
		} // End write_log

		/**
		 * Replaces all special tags in a text with the corresponding fields information and submitted data,
		 * returning an array with final text, and files submitted with the form.
		 *
		 * @param array $fields, list of form's fields.
		 * @param array $params, list of submitted fields and their values.
		 * @param string $text, the text to process replacing the fields tags by their labels and values.
		 * @param string $summary, the summary of submitted data stored in database.
		 * @param string $format, to format the output as plain text or html. Values: html, text.
		 * @param string $postid, database id of submitted data.
		 *
		 * @return array, with the elements: "text" with the formatted text, "files" with the list of submitted files.
		 */
		public static function parsing_fields_on_text($fields, $params, $text, $summary, $format, $postid)
		{
			$text = str_replace( '< %', '<%', $text );
			$attachments = array();

			// Remove empty blocks
			while( preg_match( "/<%\s*fieldname(\d+)_block\s*%>/", $text, $matches ) )
			{
				if( empty( $params[ 'fieldname'.$matches[ 1 ] ] ) )
				{
					$from = strpos( $text, $matches[ 0 ] );
					if( preg_match( "/<%\s*fieldname(".$matches[ 1 ].")_endblock\s*%>/", $text, $matches_end ) )
					{
						$lenght = strpos( $text, $matches_end[ 0 ] ) + strlen( $matches_end[ 0 ] ) - $from;
					}
					else
					{
						$lenght = strlen( $matches[ 0 ] );
					}
					$text = substr_replace( $text, '', $from, $lenght );
				}
				else
				{
					$text = preg_replace( array( "/<%\s*fieldname".$matches[ 1 ]."_block\s*%>/", "/<%\s*fieldname".$matches[ 1 ]."_endblock\s*%>/"), "", $text );
				}
			}

			$tags = self::_extract_tags( $text );

			if ( 'html' == $format )
			{
				$text = str_replace( "\n", "", $text );
				$summary = str_replace( array('&lt;', '&gt;', '\"', "\'"), array('<', '>', '"', "'" ), $summary );
			}

			// Replace the INFO tags
			if( !empty( $tags[ 'info' ] ) )
			{
				$summary_copy = $summary;
				do{
					$tmp = $summary_copy;
					$summary_copy = preg_replace(
						array(
							"/^[^\n:]*:{1,2}\s*\n/",
							"/\n[^\n:]*:{1,2}\s*\n/",
							"/\n[^\n:]*:{1,2}\s*$/"
						),
						array(
							"",
							"\n",
							""
						),
						$summary_copy
					);
				}while( $summary_copy <> $tmp );

				foreach( $tags[ 'info' ] as $tagData )
				{
					self::_single_replacement( $tagData, ( ( $tagData[ 'if_not_empty' ] ) ? $summary_copy : $summary ), $text );
				}
				unset( $tags[ 'info' ] );
			}

			foreach ($params as $item => $value)
			{
				$value_bk = $value;
				if( isset( $tags[ $item ] ) )
				{
					$label 		= ( isset( $fields[ $item ] ) && property_exists( $fields[ $item ], 'title' ) ) ? $fields[ $item ]->title : '';
					$shortlabel = ( isset( $fields[ $item ] ) && property_exists( $fields[ $item ], 'shortlabel' ) ) ? $fields[ $item ]->shortlabel : '';
					$value = ( !empty( $value ) || is_numeric( $value ) && $value == 0 ) ? ( ( is_array( $value ) ) ? implode( ", ", $value ) : $value ) : '';

					if ( 'html' == $format )
					{
						$label = str_replace( array('&lt;', '&gt;', '\"', "\'"), array('<', '>', '"', "'" ), $label );
						$shortlabel = str_replace( array('&lt;', '&gt;', '\"', "\'"), array('<', '>', '"', "'" ), $shortlabel );
						$value = str_replace( array('&lt;', '&gt;', '\"', "\'"), array('<', '>', '"', "'" ), $value );
					}

					foreach( $tags[ $item ] as $tagData )
					{
						if( $tagData[ 'if_not_empty' ] == 0 || $value !== '' )
						{
							switch( $tagData[ 'tag' ] )
							{
								case $item:
									self::_single_replacement( $tagData, $label.$tagData[ 'separator' ].$value, $text );
								break;
								case $item.'_label':
									self::_single_replacement( $tagData, $label, $text );
								break;
								case $item.'_value':
									self::_single_replacement( $tagData, $value, $text );
								break;
								case $item.'_shortlabel':
									self::_single_replacement( $tagData, $shortlabel, $text );
								break;
							}
						}
						else
						{
							$text = str_replace( $tagData[ 'node' ], '', $text );
						}
					}
					unset( $tags[ $item ] );
				}

				if( preg_match( "/_link\b/i", $item ) )
				{
					$attachments = array_merge( $attachments, $value_bk );
				}
			}

			self::_array_replacement( $tags, 'formid', ((!empty($params['formid'])) ? $params['formid'] : ''), $text );
			self::_array_replacement( $tags, 'itemnumber', ((!empty($postid)) ? $postid : ''), $text );
			self::_array_replacement( $tags, 'currentdate_mmddyyyy', date("m/d/Y H:i:s"), $text );
			self::_array_replacement( $tags, 'currentdate_ddmmyyyy', date("d/m/Y H:i:s"), $text );
			self::_array_replacement( $tags, 'ipaddress', ((!empty($fields['ipaddr'])) ? $fields['ipaddr'] : ''), $text );
			self::_array_replacement( $tags, 'subscription_id', ((!empty($params['subscr_id'])) ? $params['subscr_id'] : ''), $text );
			self::_array_replacement( $tags, 'couponcode', ((!empty($params['couponcode'])) ? $params['couponcode'] : ''), $text );
			self::_array_replacement( $tags, 'coupon', ((!empty($params['coupon'])) ? $params['coupon'] : ''), $text );

			foreach( $tags as $tagArr )
			{
				foreach( $tagArr as $tagData )
				{
					$text = str_replace( $tagData[ 'node' ], '', $text );
				}
			}

			if ( 'html' == $format )
			{
				$text = str_replace( "\n", "<br>", $text );
			}

			return array( 'text' => $text, 'files' => $attachments );
		} // End parsing_fields_on_text

		/*********************************** PRIVATE METHODS  ********************************************/

		/**
		 * Extracts all tags with the format <%...%> from the text.
		 *
		 * @since 1.0.181
		 * @param string $text, text that includes the special tags.
		 * @return array, multidimensional associative array, whose index are the tags name,
		 * and the internal arrays include the elements
		 *
		 *		- node, the literal tag.
		 *		- tag, the tag's name: fieldname#, fieldname#_label, fieldname#_value, info, ...
		 *		- if_not_empty, determines if the "if_not_empty" restriction is in the special tag or not.
		 *		- before, the value of "before" attribute in the tag.
		 *		- after, the value of "after" attribute in the tag.
		 *		- separator, the value of "separator" attribute in the tag, symbol to separate the field's label and value.
		 */
		private static function _extract_tags( $text )
		{
			$tags_arr = array();
			if(
				preg_match_all(
					"/<%(info|fieldname\d+|fieldname\d+_label|fieldname\d+_shortlabel|fieldname\d+_value|fieldname\d+_url|fieldname\d+_urls|coupon|couponcode|itemnumber|formid|subscription_id|final_price|payment_option|ipaddress|currentdate_mmddyyyy|currentdate_ddmmyyyy)\b(?:(?!%>).)*%>/i",
					$text,
					$matches
				)
			)
			{
				$tag = array();
				foreach( $matches[ 0 ] as $index => $value )
				{
					$tag[ 'node' ] = $value;
					$tag[ 'tag' ]  = strtolower( $matches[ 1 ][ $index ] );
					$tag[ 'if_not_empty' ] 	= preg_match( "/if_not_empty/i", $value );
					$tag[ 'before' ]    	= ( preg_match( "/before\s*=\s*\{\{((?:(?!\}\}).)*)\}\}/i",  $value, $match ) ) ? $match[ 1 ] : '';
					$tag[ 'after' ]   		= ( preg_match( "/after\s*=\s*\{\{((?:(?!\}\}).)*)\}\}/i", $value, $match ) ) ? $match[ 1 ] : '';
					$tag[ 'separator' ]    	= ( preg_match( "/separator\s*=\s*\{\{((?:(?!\}\}).)*)\}\}/i",  $value, $match ) ) ? $match[ 1 ] : '';

					// The base tag is the index of  $tags_arr, for the tags' names with the structure fieldname#_.
					// .., the index would be simply fieldname#
					$baseTag = ( preg_match( "/(fieldname\d+)_(label|value|shortlabel)/i", $tag[ 'tag' ], $match ) ) ? $match[ 1 ] : $tag[ 'tag' ];

					if( empty( $tags_arr[ $baseTag ] ) ) $tags_arr[ $baseTag ] = array();
					$tags_arr[ $baseTag ][] = $tag;
				}
			}
			return $tags_arr;
		} // End _extract_tags

		/**
		 * Replaces the special tags in the text by the corresponding replacements
		 *
		 * @since 1.0.181
		 * @param array reference &$tags, multidimensional associative array, whose index are the tags name,
		 * and the internal arrays include the elements
		 *
		 *		- node, the literal tag.
		 *		- tag, the tag's name: fieldname#, fieldname#_label, fieldname#_value, info, ...
		 *		- if_not_empty, determines if the "if_not_empty" restriction is in the special tag or not.
		 *		- before, the value of "before" attribute in the tag.
		 *		- after, the value of "after" attribute in the tag.
		 *		- separator, the value of "separator" attribute in the tag, symbol to separate the field's label and value.
		 * @param string $tagName, the tag's name: formi, itemnumber, currentdate_mmddyyyy, currentdate_ddmmyyyy,
		 *		ipaddress, couponcode, etc.
		 * @param string $replacement, the value uses to replace the special tag into the text.
		 * @param string reference &$text, text to be processed.
		 * @return void.
		 */
		private static function _array_replacement(&$tags, $tagName, $replacement, &$text)
		{
			if(isset($tags[ $tagName ]))
			{
				foreach( $tags[ $tagName ] as $tagData )
				{
					$text = str_replace( $tagData[ "node" ], $tagData[ "before" ].$replacement.$tagData[ "after" ], $text );
				}
				unset( $tags[ $tagName ] );
			}
		} // End _array_replacement

		/**
		 * Replaces a special tags in the text by the corresponding replacement
		 *
		 * @since 1.0.181
		 * @param array $tagData, associative array with the indexes
		 *
		 *		- node, the literal tag.
		 *		- tag, the tag's name: fieldname#, fieldname#_label, fieldname#_value, info, ...
		 *		- if_not_empty, determines if the "if_not_empty" restriction is in the special tag or not.
		 *		- before, the value of "before" attribute in the tag.
		 *		- after, the value of "after" attribute in the tag.
		 *		- separator, the value of "separator" attribute in the tag, symbol to separate the field's label and value.
		 * @param string $replacement, the value uses to replace the special tag into the text.
		 * @param string reference &$text, text to be processed.
		 * @return void.
		 */
		private static function _single_replacement($tagData, $replacement, &$text)
		{
			$text = str_replace($tagData["node"], $tagData[ "before" ].$replacement.$tagData["after"], $text);
		} // End _single_replacement

	} // End CPCFF_AUXILIARY
}