	$.fbuilder.controls[ 'fdropdownds' ] = function(){};
	$.extend(
		$.fbuilder.controls[ 'fdropdownds' ].prototype,
		$.fbuilder.controls[ 'fdropdown' ].prototype,
		$.fbuilder.controls[ 'datasource' ].prototype,
		{
			ftype:"fdropdownds",
			defaultSelection:"",
			first_time:true,
			show:function()
				{
					this.choices = [];
					this.choicesVal = [];
					return $.fbuilder.controls[ 'fdropdown' ].prototype.show.call( this );
				},
			after_show : function()
				{
					var me = this;
					$.fbuilder.controls[ 'datasource' ].prototype.getData.call( this, function( data )
						{
							var str = '',
								e 	= $( '#' + me.name );
							if( typeof data['error'] != 'undefined' )
							{
								alert( data.error );
							}
							else
							{
								var t, v, used = [];
								while(data.data.length)
								{
									var o = data.data.shift(), s = JSON.stringify(o);
									if($.inArray(s,used) == -1)
									{
										v = ( ( typeof o[ 'value' ] != 'undefined' ) ? o[ 'value' ] : '' );
										t = ( ( typeof o[ 'text' ] != 'undefined' )  ? o[ 'text' ]  :  v );
										str += '<option value="' + $.fbuilder.htmlEncode( v ) + '" vt="' + $.fbuilder.htmlEncode((me.toSubmit == 'text') ? t : v) +'">' + t + '</option>';
										used.push(s);
									}
								}
							}
							e.html( str );
							if( me.first_time )
							{
								me.first_time = false;
								$.fbuilder.controls[ 'datasource' ].prototype.setDefault.call( me );
							}
							e.change();
						}
					);
				},
			setVal : function( v )
				{
					this.defaultSelection = v;
					$.fbuilder.controls[ 'fdropdown' ].prototype.setVal.call( this, v );
				}
	});